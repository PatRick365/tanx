#pragma once

#include <chrono>

class BenchMarker
{
    using time_stamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>;

public:
    BenchMarker();

    void update(long long timeDiff);

    unsigned int fps() const;

    float timeDiff() const;

private:
    time_stamp m_timestamp;

    unsigned int m_frameCounter;
    unsigned int m_fps;

    float m_timeDiffSum;
    float m_timeDiff;

};

