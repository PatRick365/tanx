#pragma once

#include <SFML/Graphics.hpp>
#include "material.h"

class Particle
{
public:
    Particle(sf::Vector2f pos, Material::TextureId textureId);

    void update(long long timeDiff);

    bool lifeTimeFinished() const;

    long long lifeTimeEnd() const;
    long long lifeTime() const;


    sf::Vector2f pos() const;

    float rot() const;

    Material::TextureId textureId() const;

private:
    sf::Vector2f m_pos;
    float m_rot;
    bool m_rotateLeft;

    long long m_lifeTime;
    long long m_lifeTimeEnd;
    bool m_lifeTimeFinished;

    Material::TextureId m_textureId;


};
