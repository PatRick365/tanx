#ifndef MATERIAL_H
#define MATERIAL_H


class Material
{
public:
    enum TextureId
    {
        WoodPlanks,
        Wood,
        StoneBrick,
        Dirt,
        Brick,
        Gravel,
        CobbleStone,
        Stone,
        Grass,
        Bedrock,
        Tnt,
        Sand
    };


public:
    Material(float blastResistance, TextureId textureId);
    Material(TextureId textureId);

    float blastResistance() const;
    bool destructible() const;

    TextureId textureId() const;

private:
    float m_blastResistance;
    bool m_destructible;

    TextureId m_textureId;

};

#endif // MATERIAL_H
