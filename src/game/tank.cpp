#include "tank.h"
#include "../globalconst.h"
#include "functions.h"

#include "cmath"
#include "iostream"

//const float TANK_WIDTH = 40;
//const float TANK_HEIGHT = 66;

Tank::Tank(unsigned int id, float rot) :
    m_id(id), m_rot(rot) // overwritten
{
    m_pos = sf::Vector2f(150, 150);
    m_size = sf::Vector2f(static_cast<float>(GlobalConst::DIM_TANK_BODY_WIDTH), static_cast<float>(GlobalConst::DIM_TANK_BODY_HEIGHT));

    m_speedMax = 1.0f;
    m_rotSpeedMax = 0.5f;

    //m_turretRot = rot;  // overwritten in reset

    m_bulletCooldown = 1000 * 1000 ;// / 1000;

    m_turretSize = sf::Vector2f(static_cast<float>(GlobalConst::DIM_TANK_TURRET_WIDTH), static_cast<float>(GlobalConst::DIM_TANK_TURRET_HEIGHT));

    m_lastSpawnIndex = -1;

    m_playedReloadSound = true;

    m_points = 0;

    m_blowBack = false;

    m_savedRot = 0;
    m_savedTurretRot = 0;

    reset(0);
}

void Tank::reset(float rot)
{
    m_speed = 0;
    m_rot = rot;
    m_rotSpeed = 0;
    m_turretRot = rot;  // unused

    m_bulletCooldownCurrent = 0;

    m_inputMove = 0;
    m_inputShoot = false;
    m_inputRotate = 0;
    m_inputTurretRotate = 0;
}

void Tank::update(long long timeDiff)
{
    savePosition();
    saveTurretPosition();

    if (m_bulletCooldownCurrent > 0)
    {
        m_bulletCooldownCurrent -= timeDiff;
        if (m_bulletCooldownCurrent < 0)
        {
            m_playedReloadSound = false;
        }
    }

    float targetSpeed = m_speedMax * m_inputMove;

    float acceleration = timeDiff / GlobalConst::TANK_ACCELARATION;

    //std::cout << "timeDiff: " << timeDiff << ", accelaration: " << acceleration << "\n";

    if (m_speed < targetSpeed)
    {
        m_speed += acceleration;
        if (m_inputMove > 0) m_speed += (acceleration / 2) * m_inputMove;
        if (targetSpeed == 0.0f)
        {
            if (m_speed > 0) m_speed = 0;
            m_speed += acceleration * 2;
        }
        if (m_speed < -m_speedMax) m_speed = -m_speedMax;
    }
    else if (m_speed > targetSpeed)
    {
        m_speed -= acceleration;
        if (m_inputMove < 0) m_speed += (acceleration / 2) * m_inputMove;
        if (targetSpeed == 0.0f)
        {
            if (m_speed < 0) m_speed = 0;
            m_speed -= acceleration * 2;
        }
        if (m_speed > m_speedMax) m_speed = m_speedMax;
    }

    // blowback should be directional

    //egrsdstd::cout << "m_rot: " << m_rot << ", m_turretRot: " << m_turretRot << "\n";

    //if ((m_rot < m_turretRot) - 30 && (m_rot > m_turretRot + 30))
    //    if (m_blowBack) m_speed -= acceleration * 100;

    if (m_blowBack)
    {
        float dist = Functions::angleDistClockwise(m_rot, m_turretRot);
        if (dist < 35 || dist > (360 - 35)) m_speed -= acceleration * 150;
        else if (dist > 180 - 35 && dist < (180 + 35)) m_speed += acceleration * 150;
    }

    float timeFactor = timeDiff / GlobalConst::TANK_ACCELARATION * 1000;

    m_pos = Functions::moveAngular(m_pos, m_rot, m_speed * timeFactor);

    m_rot += m_inputRotate / 3 * timeFactor;
    m_turretRot += m_inputRotate / 3 * timeFactor;
    m_turretRot -= m_inputTurretRotate / 4 * timeFactor;

    if (m_turretRot > 359) m_turretRot = 0;
    else if (m_turretRot < 0) m_turretRot = 359;

    if (m_rot > 359) m_rot = 0;
    else if (m_rot < 0) m_rot = 359;

    m_blowBack = false;

    m_inputMove = 0;
    m_inputShoot = false;
    m_inputRotate = 0;
    m_inputTurretRotate = 0;
}

void Tank::savePosition()
{
    m_savedPos = m_pos;
    m_savedRot = m_rot;
}

void Tank::restorePosition()
{
    m_pos = m_savedPos;
    m_rot = m_savedRot;
    m_speed = 0;
    m_rotSpeed = 0;

    m_blowBack = false;
}
void Tank::saveTurretPosition()
{
    m_savedTurretRot = m_turretRot;
}

void Tank::restoreTurretPosition()
{
    m_turretRot = m_savedTurretRot;
}

bool Tank::wantToShoot()
{
    if (m_inputShoot && m_bulletCooldownCurrent <= 0)
    {
        m_bulletCooldownCurrent = m_bulletCooldown;
        return true;
    }
    return false;
}

std::vector<sf::Vector2f> Tank::polygonTurret()
{
    return Functions::getPolygon(sf::Vector2f(m_pos.x, m_pos.y - m_turretSize.y / 2 + m_turretSize.x / 2), m_turretSize, m_turretRot, m_pos);
}

std::vector<sf::Vector2f> Tank::polygon()
{
    return Functions::getPolygon(m_pos, m_size, m_rot);
}

void Tank::setInputTurretRotate(float inputTurretRotate)
{
    m_inputTurretRotate = inputTurretRotate;
}

sf::Vector2f Tank::turretSize() const
{
    return m_turretSize;
}

float Tank::turretRot() const
{
    return m_turretRot;
}

unsigned int Tank::id() const
{
    return m_id;
}

void Tank::setTurretRot(float turretRot)
{
    m_turretRot = turretRot;
}

float Tank::speed() const
{
    return m_speed;
}

void Tank::setSpeed(float speed)
{
    m_speed = speed;
}

int Tank::lastSpawnIndex() const
{
    return m_lastSpawnIndex;
}

void Tank::setLastSpawnIndex(int lastSpawnIndex)
{
    m_lastSpawnIndex = lastSpawnIndex;
}

void Tank::setBlowBack(bool blowBack)
{
    m_blowBack = blowBack;
}

bool Tank::playedReloadSound()
{
    if (!m_playedReloadSound)
    {
        m_playedReloadSound = true;
        return false;
    }
    return true;
}

void Tank::incrementPoints()
{
    m_points++;
}

void Tank::decrementPoints()
{
    if (m_points > 0) m_points--;
}

unsigned int Tank::points() const
{
    return m_points;
}

void Tank::setInputMove(float input)
{
    m_inputMove = input;
}

void Tank::setInputRotate(float input)
{
    m_inputRotate = input;
}

bool Tank::inputShoot() const
{
    return m_inputShoot;
}

void Tank::setInputShoot(bool inputShoot)
{
    m_inputShoot = inputShoot;
}

sf::Vector2f Tank::pos() const
{
    return m_pos;
}

void Tank::setPos(const sf::Vector2f &pos)
{
    m_pos = pos;
}

float Tank::rot() const
{
    return m_rot;
}

void Tank::setRot(float rot)
{
    m_rot = rot;
}

sf::Vector2f Tank::size() const
{
    return m_size;
}

void Tank::setSize(const sf::Vector2f &size)
{
    m_size = size;
}

void Tank::setSpeedMax(float speedMax)
{
    m_speedMax = speedMax;
}

void Tank::setRotSpeedMax(float rotSpeedMax)
{
    m_rotSpeedMax = rotSpeedMax;
}
