#include "explosion.h"

#include <iostream>

Explosion::Explosion(sf::Vector2f pos, unsigned int ownerId) : m_pos(pos), m_ownerId(ownerId)
{
    m_radius = 1;
    m_radiusEnd = 28.5f * 2;
    //m_radiusEnd = 50;

    m_damage = 10;

    m_lifeTimeEnd = 500 * 1000;
    m_lifeTime = 0;

    m_lifeTimeFinished = false;

    m_scale = 1;
}

void Explosion::update(long long timeDiff)
{
    m_lifeTime += timeDiff;

    float factor = m_lifeTime / static_cast<float>(m_lifeTimeEnd);

    m_radius = factor * m_radiusEnd;

    if (m_lifeTime >= m_lifeTimeEnd) m_lifeTimeFinished = true;
}

float Explosion::radius() const
{
    return m_radius * m_scale;
}

bool Explosion::isLifeTimeFinished() const
{
    return m_lifeTimeFinished;
}

float Explosion::scale() const
{
    return m_scale;
}

void Explosion::setScale(float scale)
{
    m_scale = scale;
}

unsigned int Explosion::ownerId() const
{
    return m_ownerId;
}

float Explosion::radiusEnd() const
{
    return m_radiusEnd;
}

float Explosion::damage()
{
    return m_damage / (m_radius / m_radiusEnd);
}

sf::Vector2f Explosion::pos() const
{
    return m_pos;
}
