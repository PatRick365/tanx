#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

class Functions
{
public:
    static std::vector<sf::Vector2f> getPolygon(sf::Vector2f pos, sf::Vector2f size, float rot, sf::Vector2f center);
    static std::vector<sf::Vector2f> getPolygon(sf::Vector2f pos, sf::Vector2f size, float rot);

    static sf::Vector2f moveAngular(sf::Vector2f pos, float rot, float distance);

    static float distanceBetween(sf::Vector2f p0, sf::Vector2f p1);

    static float angleOfLine(sf::Vector2f p0, sf::Vector2f p1);

    static float angleDistClockwise(float angle0, float angle1);

    static bool isPolygonIntersecting(std::vector<sf::Vector2f> a, std::vector<sf::Vector2f> b);
};

