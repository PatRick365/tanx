#include "shell.h"
#include "functions.h"
#include "../globalconst.h"

#include "cmath"
#include "iostream"

Shell::Shell(sf::Vector2f pos, float rot, unsigned int ownerId) : m_pos(pos), m_rot(rot), m_ownerId(ownerId)
{
    m_size = sf::Vector2f(12, 12);
    m_speed = 2;
    m_health = 2000;
}

void Shell::update(long long timeDiff)
{
    //float speed = GlobalConst::SHELL_ACCELARATION / timeDiff;
    float acceleration = timeDiff / GlobalConst::SHELL_ACCELARATION * m_speed;

    m_pos = Functions::moveAngular(m_pos, m_rot, acceleration);

    //std::cout << "timeDiff: " << timeDiff << ", acceleration: " << acceleration << "\n";

}

bool Shell::attack(long long timeDiff, float damage)
{
    float actualDamage = (timeDiff  / 200.0f) * damage;

    //std::cout << "timeDiff: " << timeDiff << ", damage: " << damage << ", actualDamage: " << actualDamage << ", health: " << m_health << "\n";

    m_health -= actualDamage;

    return (m_health <= 0);
}

std::vector<sf::Vector2f> Shell::polygon()
{
    return Functions::getPolygon(m_pos, m_size, m_rot);
}

float Shell::rot() const
{
    return m_rot;
}

float Shell::speed() const
{
    return m_speed;
}

void Shell::setSpeed(float speed)
{
    m_speed = speed;
}

unsigned int Shell::ownerId() const
{
    return m_ownerId;
}

sf::Vector2f Shell::size() const
{
    return m_size;
}

sf::Vector2f Shell::pos() const
{
    return m_pos;
}
