#include "functions.h"

//#include "math.h"
#include <cmath>

//#define M_PI 3.14159265358979323846f



std::vector<sf::Vector2f> Functions::getPolygon(sf::Vector2f pos, sf::Vector2f size, float rot, sf::Vector2f center)
{
    std::vector<sf::Vector2f> polygon
    {
        sf::Vector2f(pos.x - size.x / 2, pos.y - size.y / 2),
        sf::Vector2f(pos.x + size.x / 2, pos.y - size.y / 2),
        sf::Vector2f(pos.x + size.x / 2, pos.y + size.y / 2),
        sf::Vector2f(pos.x - size.x / 2, pos.y + size.y / 2)
    };

    for (auto &p : polygon)
    {
        p = sf::Vector2f(std::cos(rot * static_cast<float>(M_PI) / 180) * (p.x - center.x) - std::sin(rot * static_cast<float>(M_PI) / 180) * (p.y - center.y) + center.x,
                         std::sin(rot * static_cast<float>(M_PI) / 180) * (p.x - center.x) + std::cos(rot * static_cast<float>(M_PI) / 180) * (p.y - center.y) + center.y);
    }
    return polygon;
}

std::vector<sf::Vector2f> Functions::getPolygon(sf::Vector2f pos, sf::Vector2f size, float rot)
{
    return getPolygon(pos, size, rot, pos);
}

sf::Vector2f Functions::moveAngular(sf::Vector2f pos, float rot, float distance)
{
    float actualRot = rot - 90;
    if (actualRot < 0) actualRot = 360 + actualRot;

    return { static_cast<float>(pos.x + std::cos(actualRot * M_PI / 180.0) * distance),
             static_cast<float>(pos.y + std::sin(actualRot * M_PI / 180.0) * distance) };
}

float Functions::distanceBetween(sf::Vector2f p0, sf::Vector2f p1)
{
    const float xDiff = p1.x - p0.x;
    const float yDiff = p1.y - p0.y;
    return std::sqrt(xDiff * xDiff + yDiff * yDiff);
}

float Functions::angleOfLine(sf::Vector2f p0, sf::Vector2f p1)
{
    float angle = atan2f(p0.y - p1.y, p0.x - p1.x);

    angle = angle * 180.f / static_cast<float>(M_PI);

    angle -= 90;
    if (angle < 0) angle += 360;

    return angle;
}

float Functions::angleDistClockwise(float angle0, float angle1)
{
    float dist = angle1 - angle0;
    if (dist < 0) dist += 360;
    return dist;
}

// Checks if two polygons are intersecting.
bool Functions::isPolygonIntersecting(std::vector<sf::Vector2f> a, std::vector<sf::Vector2f> b)
{
    std::vector<sf::Vector2f> *polygons[2] = {&a, &b};

    for (auto polygon : polygons)
    {
        for (unsigned int i1 = 0; i1 < polygon->size(); i1++)
        {
            unsigned int i2 = (i1 + 1) % polygon->size();
            auto p1 = polygon->at(i1);
            auto p2 = polygon->at(i2);

            auto normal = sf::Vector2f(p2.y - p1.y, p1.x - p2.x);

            bool minASet = false, maxASet = false;
            float minA = 0, maxA = 0;
            for (auto p : a)
            {
                float projected = normal.x * p.x + normal.y * p.y;
                if (!minASet || projected < minA)
                {
                    minA = projected;
                    minASet = true;
                }
                if (!maxASet || projected > maxA)
                {
                    maxA = projected;
                    maxASet = true;
                }
            }

            bool minBSet = false, maxBSet = false;
            float minB = 0, maxB = 0;
            for (auto p : b)
            {
                float projected = normal.x * p.x + normal.y * p.y;
                if (!minBSet || projected < minB)
                {
                    minB = projected;
                    minBSet = true;
                }
                if (!maxBSet || projected > maxB)
                {
                    maxB = projected;
                    maxBSet = true;
                }
            }

            if (maxA < minB || maxB < minA)
                return false;
        }
    }
    return true;
}

