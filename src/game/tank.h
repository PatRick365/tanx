#pragma once

#include <SFML/Graphics.hpp>
#include <chrono>

class Tank
{
public:
    Tank(unsigned int id, float rot = 0);

    sf::Vector2f size() const;
    void setSize(const sf::Vector2f &size);

    void reset(float rot);

    std::vector<sf::Vector2f> polygon();
    std::vector<sf::Vector2f> polygonTurret();

    sf::Vector2f pos() const;
    void setPos(const sf::Vector2f &pos);
    void setSpeedMax(float speedMax);

    float rot() const;
    void setRot(float rot);
    void setRotSpeedMax(float rotSpeedMax);

    void update(long long timeDiff);

    void savePosition();
    void restorePosition();

    void saveTurretPosition();
    void restoreTurretPosition();

    void setInputMove(float input);
    void setInputRotate(float input);

    bool inputShoot() const;
    void setInputShoot(bool inputShoot);

    bool wantToShoot();
    //bool wantToRelaod();

    void setInputTurretRotate(float inputTurretRotate);

    sf::Vector2f turretSize() const;

    float turretRot() const;

    unsigned int id() const;

    void setTurretRot(float turretRot);

    float speed() const;
    void setSpeed(float speed);


    int lastSpawnIndex() const;
    void setLastSpawnIndex(int lastSpawnIndex);

    void setBlowBack(bool blowBack);


    bool playedReloadSound();

    unsigned int points() const;

    void incrementPoints();
    void decrementPoints();

private:
    unsigned int m_id;

    sf::Vector2f m_size;
    sf::Vector2f m_pos;

    float m_speed;
    float m_speedMax;

    float m_rot;
    float m_rotSpeed;
    float m_rotSpeedMax;

    bool m_blowBack;

    long long m_bulletCooldown;
    long long m_bulletCooldownCurrent;

    float m_inputMove;
    float m_inputRotate;
    float m_inputTurretRotate;
    bool m_inputShoot;

    sf::Vector2f m_savedPos;
    float m_savedRot;
    float m_savedTurretRot;

    sf::Vector2f m_turretSize;
    float m_turretRot;

    int m_lastSpawnIndex;

    bool m_playedReloadSound;

    unsigned int m_points;

};

