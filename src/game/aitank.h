#pragma once

class Game;

class AiTank
{
public:
    AiTank(Game &world, unsigned int tankId);

    void update(long long timeDiff);

    void setHitWall(bool hitWall);

    unsigned int tankId() const;

private:
    void resetShootCountdown();

private:
    Game &m_world;

    unsigned int m_tankId;

    long long m_shootCountdown;
    long long m_reverseCountdown;

    bool m_hitWall;
    bool m_reverse;

    bool m_followClosest;

};

