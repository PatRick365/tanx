#include "gamerenderer.h"
#include "../globalconst.h"
#include "functions.h"

#include <iostream>

GameRenderer::GameRenderer(sf::RenderWindow &window, Settings &settings) : m_window(window), m_settings(settings)
{
    m_textureTank.loadFromFile("resources/sprites/tank_body.png");
    m_textureTankTurret.loadFromFile("resources/sprites/tank_turret.png");
    m_texturePolygons.loadFromFile("resources/sprites/polygons.png");
    m_textureShell.loadFromFile("resources/sprites/shell.png");
    m_textureExplosion.loadFromFile("resources/sprites/explosion.png");
    m_textureBackground.loadFromFile("resources/sprites/grass.png");

    m_fontMexicanTequila.loadFromFile("resources/fonts/MexicanTequila.ttf");
    m_fontCapture_it.loadFromFile("resources/fonts/Capture_it.ttf");

    sf::View view(sf::FloatRect(sf::Vector2f(0, 0), static_cast<sf::Vector2f>(settings.resolution())));
    m_window.setView(view);
    //m_window.setView(sf::View(sf::FloatRect(0, 0, 1920, 1080)));

    // debug rotation
        /*float offset = 0;
        float radius = 100;

        sf::Vector2f center(radius + offset, radius + offset);

        sf::CircleShape circle(radius);
        sf::CircleShape vc(1);
        vc.setPosition(center);
        vc.setFillColor(sf::Color::White);

        m_window.draw(vc);

        std::cout << "-------------------------------------\n";
        for (unsigned int i = 0; i < circle.getPointCount(); ++i)
        {
            //std::cout << Functions::angleDirTo(v, c.getPoint(i)) << "\n";
            sf::Vector2f circlePoint = sf::Vector2f(circle.getPoint(i).x + offset, circle.getPoint(i).y + offset);
            std::cout << i << ": " << Functions::angleDirTo(center, circlePoint) << "\n";
            vc.setPosition(circlePoint);
            sf::Color col = sf::Color::White;

            if (i == 0) col = sf::Color::Red;
            if (i == 29) col = sf::Color::Blue;
            if (i == 15) col = sf::Color::Yellow;

            vc.setFillColor(col);
            m_window.draw(vc);

        }
        std::cout << "-------------------------------------\n";*/
    // debug


}

void GameRenderer::update(Game &world)
{
    m_window.clear();

    sf::Vector2f scale(m_settings.resolution().x / static_cast<float>(GlobalConst::DIM_WORLD_WIDTH),
                       m_settings.resolution().y / static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT));

    //sf::Vector2f scale(1, 1);

    sf::RectangleShape backgroundRect(sf::Vector2f(static_cast<float>(GlobalConst::DIM_WORLD_WIDTH), static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT)));
    backgroundRect.setPosition(0, 0);
    backgroundRect.setFillColor(sf::Color(0, 0, 0));
    backgroundRect.setScale(scale);
    m_window.draw(backgroundRect);

    sf::Sprite polySprite(m_texturePolygons);
    polySprite.setScale(scale);
    sf::Sprite tankSprite(m_textureTank);
    tankSprite.setScale(scale);
    sf::Sprite tankTurretSprite(m_textureTankTurret);
    tankTurretSprite.setScale(scale);
    sf::Sprite shellSprite(m_textureShell);
    shellSprite.setScale(scale);

    sf::Sprite explosionSprite(m_textureExplosion);
    //sf::Vector2f explosionScale(1.2f * scale.x, 1.2f * scale.x);
    //sf::Vector2f explosionScale(1.2f, 1.2f);
    //explosionSprite.setScale(explosionScale);   // arbitrary
    explosionSprite.setScale(scale.x * 2, scale.y * 2);


    sf::ConvexShape convexShape;
    convexShape.setTexture(&m_texturePolygons);

    sf::CircleShape circleShape;
    circleShape.setFillColor(sf::Color::Red);

    sf::ConvexShape particleShape;
    particleShape.setTexture(&m_texturePolygons);
    particleShape.setPointCount(3);
    particleShape.setPoint(0, sf::Vector2f(0, 0));
    particleShape.setPoint(1, sf::Vector2f(7, 14));
    particleShape.setPoint(2, sf::Vector2f(14, 0));
    particleShape.setScale(scale);
    particleShape.setOrigin(7, 7);

    sf::RectangleShape background;
    m_textureBackground.setRepeated(true);
    //background.setSize(sf::Vector2f(static_cast<float>(m_settings.resolution().x), static_cast<float>(m_settings.resolution().y)));
    //background.setTextureRect(sf::IntRect(0, 0, static_cast<int>(m_settings.resolution().x), static_cast<int>(m_settings.resolution().y)));

    background.setSize(sf::Vector2f(static_cast<float>(GlobalConst::DIM_WORLD_WIDTH) * scale.x, static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT) * scale.y));
    background.setTextureRect(sf::IntRect(0, 0, static_cast<int>(GlobalConst::DIM_WORLD_WIDTH), static_cast<int>(static_cast<int>(GlobalConst::DIM_WORLD_HEIGHT))));

    background.setTexture(&m_textureBackground);
    m_window.draw(background);

    sf::Text pointsText;
    pointsText.setFont(m_fontCapture_it);
    pointsText.scale(scale);

    // draw polygons
    for (const auto &poly : world.polygons())
    {
        if (!poly.isVisible()) continue;

        convexShape.setTextureRect(getTextureRect(poly.material()->textureId()));
        convexShape.setPointCount(poly.points().size());

        for (unsigned int i = 0; i < poly.points().size(); ++i)
        {
            convexShape.setPoint(i, sf::Vector2f(poly.points().at(i).x * scale.x, poly.points().at(i).y * scale.y));
        }
        m_window.draw(convexShape);
    }

    // draw tank
    for (const auto &tank : world.tanks())
    {
        //sf::Color color = (tank.id() == 0) ? sf::Color::Red : sf::Color::Blue;
        //sf::Color color = (tank.id() == 0) ? sf::Color(128, 255, 255) : sf::Color(255, 128, 255);
        //sf::Color color = (tank.id() == 0) ? sf::Color(128, 0, 0) : sf::Color(0, 0, 255);



        sf::Vector2f origin(tank.size().x / 2, tank.size().y / 2);
        tankSprite.setPosition(tank.pos().x * scale.x, tank.pos().y * scale.y);
        tankSprite.setOrigin(origin);
        tankSprite.setRotation(tank.rot());
        tankSprite.setColor(getPlayerColor(tank.id()));
        m_window.draw(tankSprite);

        sf::Vector2f originTurret(tank.turretSize().x / 2, tank.turretSize().y - tank.turretSize().x / 2);
        tankTurretSprite.setPosition(tank.pos().x * scale.x, tank.pos().y * scale.y);
        tankTurretSprite.setOrigin(originTurret);
        tankTurretSprite.setRotation(tank.turretRot());
        tankTurretSprite.setColor(getPlayerColor(tank.id()));
        m_window.draw(tankTurretSprite);

        //auto poly = tank.polygonTurret();sf::RectangleShape p0(sf::Vector2f(1, 1));p0.setPosition(poly.at(0));sf::RectangleShape p1(sf::Vector2f(1, 1));p1.setPosition(poly.at(1));sf::RectangleShape p2(sf::Vector2f(1, 1));p2.setPosition(poly.at(2));sf::RectangleShape p3(sf::Vector2f(1, 1));p3.setPosition(poly.at(3));m_window.draw(p0);m_window.draw(p1);m_window.draw(p2);m_window.draw(p3);
        //auto polyT = tank.polygon();sf::RectangleShape p0T(sf::Vector2f(1, 1));p0T.setPosition(polyT.at(0).x * scale.x, polyT.at(0).y * scale.y);sf::RectangleShape p1T(sf::Vector2f(1, 1));p1T.setPosition(polyT.at(1).x * scale.x, polyT.at(1).y * scale.y);sf::RectangleShape p2T(sf::Vector2f(1, 1));p2T.setPosition(polyT.at(2).x * scale.x, polyT.at(2).y * scale.y);sf::RectangleShape p3T(sf::Vector2f(1, 1));p3T.setPosition(polyT.at(3).x * scale.x, polyT.at(3).y * scale.y);m_window.draw(p0T);m_window.draw(p1T);m_window.draw(p2T);m_window.draw(p3T);
        //std::cout << "p0: " << p0.getPosition().x << ", " << p0.getPosition().y << " - p1: " << p1.getPosition().x << ", " << p1.getPosition().y << " - p2: " << p2.getPosition().x << ", " << p2.getPosition().y << " - p3: " << p3.getPosition().x << ", " << p3.getPosition().y << "\n";
    }

    // draw shells
    for (const auto &shell : world.shells())
    {
        shellSprite.setPosition(shell.pos().x * scale.x, shell.pos().y  * scale.y);
        shellSprite.setOrigin(shell.size().x / 2, shell.size().y / 2);
        shellSprite.setRotation(shell.rot());
        m_window.draw(shellSprite);

        //auto polyS = shell.polygon();sf::RectangleShape p0S(sf::Vector2f(1, 1));p0S.setPosition(polyS.at(0));sf::RectangleShape p1S(sf::Vector2f(1, 1));p1S.setPosition(polyS.at(1));sf::RectangleShape p2S(sf::Vector2f(1, 1));p2S.setPosition(polyS.at(2));sf::RectangleShape p3S(sf::Vector2f(1, 1));p3S.setPosition(polyS.at(3));m_window.draw(p0S);m_window.draw(p1S);m_window.draw(p2S);m_window.draw(p3S);
    }

    // draw explosions
    for (const auto &explosion : world.explosions())
    {
        explosionSprite.setTextureRect(getTextureExplosion(explosion.radius() / explosion.radiusEnd()));
        //explosionSprite.setPosition((explosion.pos().x - explosion.radiusEnd()) * scale.x * 2, (explosion.pos().y - explosion.radiusEnd())  * scale.y * 2);
        explosionSprite.setPosition((explosion.pos().x - explosion.radiusEnd()) * scale.x, (explosion.pos().y - explosion.radiusEnd()) * scale.y);
        m_window.draw(explosionSprite);

        // explosion.scale() rendereng not quite working
        //circleShape.setPosition((explosion.pos().x - explosion.radius()) * scale.x, (explosion.pos().y - explosion.radius())  * scale.y);
        //circleShape.setRadius(explosion.radius() * scale.x);
        //m_window.draw(circleShape);
    }

    for (const auto &particle : world.particles())
    {
        particleShape.setTextureRect(getTextureRect(particle.textureId()));
        particleShape.setPosition(particle.pos().x * scale.x, particle.pos().y * scale.y);
        particleShape.setRotation(particle.rot());
        particleShape.setFillColor(sf::Color(255, 255, 255, 255 - static_cast<unsigned char>(255 * particle.lifeTime() / particle.lifeTimeEnd())));
        m_window.draw(particleShape);
    }

    if (m_settings.showFps())
    {
        sf::Text frameTimeText(std::to_string(world.benchMarker().fps()), m_fontMexicanTequila, 24);
        frameTimeText.setFillColor(sf::Color::Yellow);
        m_window.draw(frameTimeText);
    }

    for (unsigned int i = 0; i < world.tanks().size(); ++i)
    {
        pointsText.setString(std::to_string(world.tanks().at(i).points()));
        sf::Vector2f pos;
        pos.x = static_cast<float>(m_settings.resolution().x / 3.0f * ((i % 2) + 1));
        pos.y = (i > 1) ? static_cast<float>(m_settings.resolution().y - pointsText.getLocalBounds().height) : 0;
        pointsText.setPosition(pos);
        pointsText.setFillColor(getPlayerColor(world.tanks().at(i).id()));
        m_window.draw(pointsText);
    }

    // draw aim line
    /*sf::RectangleShape rect(sf::Vector2f(1, 200));
    float angleDir = Functions::angleOfLine(world.tanks().at(0).pos(), world.tanks().at(1).pos());
    rect.setPosition(world.tanks().at(0).pos().x * scale.x, world.tanks().at(0).pos().y * scale.y);
    rect.setRotation(angleDir);
    //std::cout << angleDir << "\n";
    rect.setFillColor(sf::Color::White);
    m_window.draw(rect);*/

    //frameTimeText.setString(std::to_string(world.benchMarker().timeDiff()));
    //frameTimeText.setPosition(m_settings.resolution().x - 200.0f, 0);
    //m_window.draw(frameTimeText);
    m_window.display();
}

sf::IntRect GameRenderer::getTextureRect(Material::TextureId textureId)
{
    int size = 16;

    switch (textureId)
    {
    case Material::WoodPlanks: return { 64, 0, size, size };
    case Material::Wood: return { 80, 16, size, size };
    case Material::StoneBrick: return { 96, 48, size, size };
    case Material::Dirt: return { 32, 0, size, size };
    case Material::Brick: return { 112, 0, size, size };
    case Material::Gravel: return { 48, 16, size, size };
    case Material::CobbleStone: return { 0, 16, size, size };
    case Material::Stone: return { 16, 0, size, size };
    case Material::Grass: return { 0, 0, size, size };
    case Material::Bedrock: return { 16, 16, size, size };
    case Material::Tnt: return { 160, 0, size, size };
    case Material::Sand: return { 32, 16, size, size };
    }
    return { 0, 0, size, size };
}

sf::IntRect GameRenderer::getTextureExplosion(float progress)
{
    int stage = static_cast<int>(progress * 10);
    int size = 56;

    //std::cout << "stage: " << stage << ", progress: " << progress << "\n";

    switch (stage)
    {
    case 0: return { 1, 1, size, size };
    case 1: return { 61, 1, size, size };
    case 2: return { 121, 1, size, size };
    case 3: return { 181, 1, size, size };
    case 4: return { 241, 1, size, size };
    case 5: return { 1, 61, size, size };
    case 6: return { 61, 61, size, size };
    case 7: return { 121, 61, size, size };
    case 8: return { 181, 61, size, size };
    case 9: return { 241, 61, size, size };
    }
    return { 1, 1, size, size };
}

sf::Color GameRenderer::getPlayerColor(unsigned int id)
{
    switch (id)
    {
    case 0: return  { 128, 255, 255 };
    case 1: return  { 255, 128, 255 };
    case 2: return  { 255, 255, 128 };
    case 3: return  { 255, 128, 128 };
    default: return { 255, 255, 255 };
    }
}


