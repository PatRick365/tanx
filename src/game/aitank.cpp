#include "aitank.h"
#include "game.h"
#include "tank.h"
#include "functions.h"

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iostream>


AiTank::AiTank(Game &world, unsigned int tankId)
    : m_world(world), m_tankId(tankId)
{
    srand(static_cast<unsigned int>(time(nullptr)));
    resetShootCountdown();

    m_reverseCountdown = 0;
    m_shootCountdown = 0;

    m_hitWall = false;
    m_reverse = false;

    m_followClosest = (m_world.tankById(tankId)->id() % 2);
}

void AiTank::update(long long timeDiff)
{
    m_shootCountdown -= timeDiff;

    Tank *tank = m_world.tankById(m_tankId);
    assert(tank != nullptr);

    Tank *tankOther = nullptr;
    for (auto &tanky : m_world.tanks())
    {
        if (tanky.id() != tank->id())
        {
            if (tankOther == nullptr)
            {
                tankOther = &tanky;
                continue;
            }
            bool distGreater = (Functions::distanceBetween(tank->pos(), tanky.pos()) > Functions::distanceBetween(tank->pos(), tankOther->pos()));
            if (m_followClosest)
            {
                if (distGreater) tankOther = &tanky;
            }
            else
            {
                if (!distGreater) tankOther = &tanky;
            }


        }
    }
    if (tankOther == nullptr) return;

    // rotate to aim at tank
    float targetRot = Functions::angleOfLine(tank->pos(), tankOther->pos());

    if (Functions::angleDistClockwise(tank->turretRot(), targetRot) > 180)
        tank->setInputTurretRotate(1);
    else
        tank->setInputTurretRotate(-1);

    if (Functions::angleDistClockwise(tank->rot(), targetRot) > 180)
        tank->setInputRotate(-1);
    else
        tank->setInputRotate(1);

    float distAngleTurret = Functions::angleDistClockwise(tank->turretRot(), targetRot);
    if ((distAngleTurret < 2 || distAngleTurret > (360 - 2)))
        tank->setInputTurretRotate(0);

    float distAngle = Functions::angleDistClockwise(tank->rot(), targetRot);
    if ((distAngle < 0.5f || distAngle > (360 - 0.5f)))
        tank->setInputRotate(0);


    if (m_shootCountdown <= 0)
    {
        tank->setInputShoot(true);
        resetShootCountdown();
    }

    float dist = Functions::distanceBetween(tank->pos(), tankOther->pos());

    float moveDir = 0;

    if (dist > 600)
        moveDir = 1;
    if (dist > 450)
        if (!(rand() % 3)) moveDir = 1;
    if (dist > 300)
        if (!(rand() % 10)) moveDir = 1;
    if (dist < 300)
        moveDir = -1;

    if (m_hitWall)
    {
        m_reverse = !m_reverse;
        m_hitWall = false;
        m_reverseCountdown = 750 * 1000;
        resetShootCountdown();
    }

    if (m_reverse) m_reverseCountdown -= timeDiff;

    if (m_reverseCountdown < 0)
    {
        m_reverse = false;
        m_reverseCountdown = 750 * 1000;
        m_shootCountdown = -1;
    }

    if (m_reverse) moveDir *= -1;

    tank->setInputMove(moveDir);
}

void AiTank::resetShootCountdown()
{
    m_shootCountdown = (rand() % 5 + 1) * 1000 * 1000;
}

unsigned int AiTank::tankId() const
{
    return m_tankId;
}

void AiTank::setHitWall(bool hitWall)
{
    m_hitWall = hitWall;
}
