#pragma once

#include "game.h"
#include "../settings.h"

#include <SFML/Graphics.hpp>

class GameRenderer
{
public:
    GameRenderer(sf::RenderWindow &window, Settings &settings);

    void update(Game &world);


private:
    sf::IntRect getTextureRect(Material::TextureId textureId);
    sf::IntRect getTextureExplosion(float progress);
    sf::Color getPlayerColor(unsigned int id);

private:
    sf::RenderWindow &m_window;
    Settings &m_settings;

    sf::Texture m_textureTank;
    sf::Texture m_textureTankTurret;
    sf::Texture m_texturePolygons;
    sf::Texture m_textureShell;
    sf::Texture m_textureExplosion;
    sf::Texture m_textureBackground;

    sf::Font m_fontMexicanTequila;
    sf::Font m_fontCapture_it;



};

