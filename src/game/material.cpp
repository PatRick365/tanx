#include "material.h"

#include <iostream>

Material::Material(float blastResistance, TextureId textureId)
    : m_blastResistance(blastResistance), m_textureId(textureId)
{
    m_destructible = true;
}

Material::Material(TextureId textureId) : m_textureId(textureId)
{
    m_blastResistance = 0;
    m_destructible = false;
}

float Material::blastResistance() const
{
    return m_blastResistance;
}

bool Material::destructible() const
{
    return m_destructible;
}

Material::TextureId Material::textureId() const
{
    return m_textureId;
}
