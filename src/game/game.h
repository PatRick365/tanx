#pragma once

#include "../globalconst.h"
#include "../soundmanager.h"
#include "tank.h"
#include "shell.h"
#include "../settings.h"
#include "../benchmarker.h"
#include "polygon.h"
#include "polygonfactory.h"
#include "explosion.h"
#include "aitank.h"
#include "particle.h"

#include <vector>
#include <array>
#include <set>
#include <chrono>

class Application;

class Game
{
public:
    using time_stamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>;

    Game(sf::RenderWindow &window, Settings &settings, SoundManager &soundManager, int numPlayers, int numPlayersHuman);

    void update();

    std::vector<Tank> &tanks();
    std::vector<Shell> &shells();
    std::vector<Polygon> &polygons();
    std::vector<Explosion> &explosions();
    std::vector<Particle> &particles();

    Tank *tankById(unsigned int id);

    const BenchMarker &benchMarker() const;
    bool stopGameFlag() const;
    bool isCollidedPolygon(const std::vector<sf::Vector2f> &polygon);


public:
    int m_frameTime;


private:
    int isCollidedTank(const std::vector<sf::Vector2f> &polygon);
    Tank *isCollidedTank(sf::Vector2f pos, float radius);
    std::vector<Polygon *> isCollidedPolygon(sf::Vector2f pos, float radius);
    bool isOutOfBounds(std::vector<sf::Vector2f> polygon);

    void respawnTank(Tank &tank);
    void spawnParticles(sf::Vector2f pos, unsigned int multiplier = 1, Material::TextureId textureId = Material::Bedrock);

private:
    bool m_stopGameFlag;

    sf::RenderWindow &m_window;
    SoundManager &m_soundManager;
    Settings &m_settings;
    PolygonFactory m_polygonFactory;

    std::set<sf::Keyboard::Key> m_pressedKeys;
    std::set<sf::Keyboard::Key> m_releasedKeys;

    std::vector<Tank> m_tanks;
    std::vector<Shell> m_shells;
    std::vector<Polygon> m_polygons;
    std::vector<Explosion> m_explosions;
    std::vector<Particle> m_particles;
    std::vector<AiTank> m_ais;

    std::vector<sf::Vector2f> m_spawnPoints;

    time_stamp m_timestamp;

    BenchMarker m_benchMarker;

    //int m_numPlayers;
    int m_numPlayersHuman;

};

