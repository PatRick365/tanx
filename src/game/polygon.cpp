#include "polygon.h"

#include <iostream>
#include <utility>

//Polygon::Polygon(std::vector<sf::Vector2f> points, Material *material) : points(points), m_material(material)
Polygon::Polygon(std::vector<sf::Vector2f> points, Material *material) : m_points(std::move(points)), m_material(material)
{
    //std::move();



    m_health = 1000;
    m_isVisible = true;
}

/*Polygon::Polygon()
{
    m_isVisible = true;
}*/

bool Polygon::attack(long long timeDiff, float damage)
{
    if (!m_material->destructible()) return false;

    float actualDamage = (timeDiff  / 2.0f) * damage / m_material->blastResistance();

    //std::cout << "actualDamage: " << actualDamage << "\n";
    //std::cout << "timeDiff: " << timeDiff << ", damage: " << damage << ", actualDamage: " << actualDamage <<"\n";

    m_health -= actualDamage;

    if (m_health <= 0)
    {
        m_isVisible = false;
        return true;
    }
    return false;
}

const Material *Polygon::material() const
{
    return m_material;
}

const std::vector<sf::Vector2f> &Polygon::points() const
{
    return m_points;
}

void Polygon::setIsVisible(bool isVisible)
{
    m_isVisible = isVisible;
}

bool Polygon::isVisible() const
{
    return m_isVisible;
}


