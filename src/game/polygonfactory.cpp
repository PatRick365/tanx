#include "polygonfactory.h"
#include "../globalconst.h"

#include <iostream>
#include <cstdlib>     /* srand, rand */
#include <ctime>       /* time */
#include <random>
#include <algorithm>

PolygonFactory::PolygonFactory() //: m_materials( {Material(1000), Material()} )
{
    //srand(static_cast<unsigned int>(time(nullptr)));
    m_materials = std::vector<Material>({
        Material(400, Material::WoodPlanks),
        Material(400, Material::Wood),
        Material(1500, Material::StoneBrick),
        Material(200, Material::Dirt),
        Material(300, Material::Brick),
        Material(200, Material::Gravel),
        Material(800, Material::CobbleStone),
        Material(1800, Material::Stone),
        Material(500, Material::Grass),
        Material(Material::Bedrock),
        Material(200, Material::Tnt),
        Material(200, Material::Sand)
    });


    /*
     *  Material woodPlanks(400, Material::WoodPlanks);
    Material wood(400, Material::Wood);
    Material stoneBrick(1500, Material::StoneBrick);
    Material dirt(200, Material::Dirt);
    Material brick(300, Material::Brick);
    Material gravel(200, Material::Gravel);
    Material cobbleStone(800, Material::CobbleStone);
    Material stone(1800, Material::Stone);
    Material grass(500, Material::Grass);
    Material bedrock(Material::Bedrock);
    Material tnt(200, Material::Tnt);
    */

    //m_materials = { woodPlanks, wood, stoneBrick, dirt, brick, gravel, cobbleStone, stone, tnt };
}

std::vector<Polygon> PolygonFactory::LoadInitialState()
{
    //return std::vector<Polygon>();

    const sf::Vector2f tileSize(GlobalConst::DIM_WORLD_WIDTH / 160.0f * 2.0f, GlobalConst::DIM_WORLD_HEIGHT / 90.0f * 2.0f);

    std::vector<Polygon> polygons;

    int ww = 81;
    int wh = 46;

    setBunker(polygons, 0, 0, { DownRight });
    setBunker(polygons, ww - 18, 0, { DownlLeft });
    setBunker(polygons, 0, wh - 18, { UpRight });
    setBunker(polygons, ww - 18, wh - 18, { UpLeft });


    //for (int i = 1; i < 6; ++i)
    //    setRect(polygons, ww / 2 - i, wh / 2 - i, i * 2, Material::Dirt, {});

    //setBlock(polygons, 20, 3, Material::Tnt);

    int xStart = 31 - 7;
    int yStart = 14 - 7;
    int xEnd = 48 + 7;
    int yEnd = 31 + 7;

    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, static_cast<int>(m_materials.size()) - 1);

    auto materialIdx = static_cast<unsigned int>(uniform_dist(e1));;

    for (int yp = yStart; yp < yEnd; ++yp)
    {
        for (int xp = xStart; xp < xEnd; ++xp)
        {
            if (uniform_dist(e1) / 5) continue;
            if (uniform_dist(e1) / 5) materialIdx = static_cast<unsigned int>(uniform_dist(e1));

            //if (yp > endY - 5 && xp > endX - 5) continue;
            //if ((xp > endX / 100 && yp > endY / 100)) continue;
            auto material = static_cast<Material::TextureId>(materialIdx);
            if (material == Material::Bedrock || material == Material::Grass) material = Material::Tnt;
            setBlock(polygons, xp, yp, material);
            //setBlock(polygons, xp, yp, Material::Sand);
            //setBlock(polygons, xp, yp, Material::StoneBrick);
            //setBlock(polygons, xp, yp, Material::Tnt);
        }
    }

    //setBunker(polygons, ww / 2 - 9, wh / 2 - 9, { DownRight, DownlLeft, UpRight, UpLeft });


    /*
    // 0:u, 1:d, 2:l, 3:r
    int houseSize = 11;
    //                 x                     y                     size           doorside
    setHouse(polygons, 1,                    1,                    houseSize + 2, 3);
    setHouse(polygons, ww - (houseSize + 2), 1,                    houseSize,     1);
    setHouse(polygons, ww - (houseSize + 2), wh - (houseSize + 2), houseSize,     2);
    setHouse(polygons, 1,                    wh - (houseSize + 2), houseSize,     0);


    for (int y = 20; y < 29; ++y)
    {
        for (int x = 20; x < 29; ++x)
        {
            setBlock(polygons, x, y, ((x + y) % 2) ? Material::Gravel : Material::StoneBrick);
        }
    }
    */

    /*for (int y = 10; y < 39; ++y)
    {
        for (int x = 40; x < 49; ++x)
        {
            setBlock(polygons, x, y, Material::Gravel);
        }
    }*/

    /*for (int y = 10; y < 39; ++y)
    {
            setBlock(polygons, 15, y, Material::Gravel);
    }
*/
    return polygons;
}

void PolygonFactory::setBlock(std::vector<Polygon> &polygons, int x, int y, Material::TextureId material)
{
    const sf::Vector2f tileSize(GlobalConst::DIM_WORLD_WIDTH / 160.0f * 2.0f, GlobalConst::DIM_WORLD_HEIGHT / 90.0f * 2.0f);

    //std::vector<Polygon> rect;

    /*if (rand() % 2)
        rect = getRightToLeftRect(sf::Vector2f(tileSize.x * x, tileSize.y * y), tileSize, m_materials.at(textureId));
    else
        rect = getLeftToRightRect(sf::Vector2f(tileSize.x * x, tileSize.y * y), tileSize, m_materials.at(textureId));*/


    sf::Vector2f p0(tileSize.x * x,                    tileSize.y * y);
    sf::Vector2f p1(tileSize.x * x + tileSize.x,       tileSize.y * y);
    sf::Vector2f p2(tileSize.x * x + tileSize.x,       tileSize.y * y + tileSize.y);
    sf::Vector2f p3(tileSize.x * x,                    tileSize.y * y + tileSize.y);
    sf::Vector2f p4((tileSize.x * x + tileSize.x / 2), (tileSize.y * y + tileSize.y / 2));

    std::vector<Polygon> rect = std::vector<Polygon> {
        Polygon( { p0, p1, p4 }, &m_materials.at(material) ),
        //Polygon( { p4, p1, p2 }, &m_materials.at(material) ),
        Polygon( { p1, p2, p4 }, &m_materials.at(material) ),
        Polygon( { p3, p4, p2 }, &m_materials.at(material) ),
        Polygon( { p0, p4, p3 }, &m_materials.at(material) )
    };


    polygons.insert(polygons.end(), rect.begin(), rect.end());
}

void PolygonFactory::setBunker(std::vector<Polygon> &polygons, int x, int y, const std::vector<Side> &doorSides)
{
    int size = 17;
    setRect(polygons, x, y, size, Material::StoneBrick, doorSides);
    setRect(polygons, x + 1, y + 1, size - 2, Material::CobbleStone, doorSides);
    setRect(polygons, x + 2, y + 2, size - 4, Material::StoneBrick, doorSides);

            /*int size = 17;
    for (int yp = 0; yp < size; ++yp)
        for (int xp = 0; xp < size; ++xp)
            if (((yp == 0) || (xp == 0) || (yp == size - 1) || (xp == size - 1)))
                //if (!(yp < size && xp > size - 2))
                    setBlock(polygons, x + xp , y + yp, Material::StoneBrick);
    size = 15;
    for (int yp = 0; yp < size; ++yp)
        for (int xp = 0; xp < size; ++xp)
            if (((yp == 0) || (xp == 0) || (yp == size - 1) || (xp == size - 1)))
                //if (!(yp == size - 1 && xp == size - 1))
                    setBlock(polygons, x + xp + 1, y + yp + 1, Material::Gravel);
    size = 13;
    for (int yp = 0; yp < size; ++yp)
        for (int xp = 0; xp < size; ++xp)
            if (((yp == 0) || (xp == 0) || (yp == size - 1) || (xp == size - 1)))
                //if (!(yp == size - 1 && xp == size - 1))
                    setBlock(polygons, x + xp + 2, y + yp + 2, Material::StoneBrick);*/



}

void PolygonFactory::setRect(std::vector<Polygon> &polygons, int x, int y, int size, Material::TextureId material, std::vector<Side> doorSides)
{
    for (int yp = 0; yp < size; ++yp)
    {
        for (int xp = 0; xp < size; ++xp)
        {
            if (((yp == 0) || (xp == 0) || (yp == size - 1) || (xp == size - 1)))
            {
                if (std::count(doorSides.begin(), doorSides.end(), DownRight) && xp > size - 4 && yp > size - 4) continue;
                if (std::count(doorSides.begin(), doorSides.end(), DownlLeft) && xp < 3 && yp > size - 4) continue;
                if (std::count(doorSides.begin(), doorSides.end(), UpRight) && xp > size - 4 && yp < 3) continue;
                if (std::count(doorSides.begin(), doorSides.end(), UpLeft) && xp < 3 && yp < 3) continue;
                setBlock(polygons, x + xp + 0, y + yp + 0, material);
            }
        }
    }
}

void PolygonFactory::setHouse(std::vector<Polygon> &polygons, int x, int y, int size, int doorSide)
{
    size--;

    setBlock(polygons,  x,        y,        Material::Wood);
    setBlock(polygons,  x + size, y,        Material::Wood);
    setBlock(polygons,  x,        y + size, Material::Wood);
    setBlock(polygons,  x + size, y + size, Material::Wood);

    if (doorSide == -1) doorSide = rand() % 4;

    //doorSide = 0;

    for (int i = 0; i < size - 1; ++i)
    {
        if (doorSide != 0 || !(i >= size / 3 && i < size - (size + 1) / 3))
            setBlock(polygons, x + 1 + i, y, Material::StoneBrick);
        if (doorSide != 1 || !(i >= size / 3 && i < size - (size + 1) / 3))
            setBlock(polygons, x + 1 + i, y + size, Material::StoneBrick);
        if (doorSide != 2 || !(i >= size / 3 && i < size - (size + 1) / 3))
            setBlock(polygons, x, y + 1 + i, Material::StoneBrick);
        if (doorSide != 3 || !(i >= size / 3 && i < size - (size + 1) / 3))
            setBlock(polygons, x + size, y + 1 + i, Material::StoneBrick);
    }



}

/*
std::vector<Polygon> PolygonFactory::LoadInitialState()
{
    std::vector<Polygon> polygons;

    const sf::Vector2f tileSize(GlobalConst::DIM_WORLD_WIDTH / 160 * 2.0f, GlobalConst::DIM_WORLD_HEIGHT / 90 * 2.0f);

    auto wall0 = getWall(sf::Vector2f(tileSize.x * 10, tileSize.y * 10), tileSize, 60, 3, true);
    auto wall1 = getWall(sf::Vector2f(tileSize.x * 10, tileSize.y * (45 - 10 - 4)), tileSize, 60, 3, true);

    auto wall2 = getWall(sf::Vector2f(tileSize.x * 10, tileSize.y * (45 /2 - 1)), tileSize, 60, 2, true);

    auto wall3 = getWall(sf::Vector2f(tileSize.x * 12, tileSize.y * (13)), tileSize, 1, 8);
    auto wall4 = getWall(sf::Vector2f(tileSize.x * (80 - 13), tileSize.y * (13)), tileSize, 1, 8);

    auto wall5 = getWall(sf::Vector2f(tileSize.x * 12, tileSize.y * (45 - 22)), tileSize, 1, 8);
    auto wall6 = getWall(sf::Vector2f(tileSize.x * (80 - 13), tileSize.y * (45 - 22)), tileSize, 1, 8);

    polygons.insert(polygons.end(), wall0.begin(), wall0.end());
    polygons.insert(polygons.end(), wall1.begin(), wall1.end());
    polygons.insert(polygons.end(), wall2.begin(), wall2.end());
    polygons.insert(polygons.end(), wall3.begin(), wall3.end());
    polygons.insert(polygons.end(), wall4.begin(), wall4.end());
    polygons.insert(polygons.end(), wall5.begin(), wall5.end());
    polygons.insert(polygons.end(), wall6.begin(), wall6.end());

    return polygons;
}

std::vector<Polygon> PolygonFactory::getWall(const sf::Vector2f &pos, const sf::Vector2f &tileSize, unsigned int width, unsigned int heigth, bool roundEdges)
{
    std::vector<Polygon> polygons;
    polygons.reserve(width * heigth * 2);

    for (unsigned int y = 0; y < heigth; ++y)
    {
        for (unsigned int x = 0; x < width; ++x)
        {
            if (roundEdges)
            {
                if (x == 0 && y == 0)
                {
                    auto r = getRightToLeftRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
                    polygons.push_back(r.at(1));
                    continue;
                }
                else if (x == width - 1 && y == 0)
                {
                    auto r = getLeftToRightRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
                    polygons.push_back(r.at(0));
                    continue;
                }
                else if (x == 0 && y == heigth - 1)
                {
                    auto r = getLeftToRightRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
                    polygons.push_back(r.at(1));
                    continue;
                }
                else if (x == width - 1 && y == heigth - 1)
                {
                    auto r = getRightToLeftRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
                    polygons.push_back(r.at(0));
                    continue;
                }
            }

            std::vector<Polygon> rect;
            if (rand() % 2)
            {
                rect = getLeftToRightRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
            }
            else
            {
                rect = getRightToLeftRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize);
            }
            polygons.insert(polygons.end(), rect.begin(), rect.end());
        }
    }
    //polygons.erase(polygons.begin(), polygons.begin() + 1);

    return polygons;
}
*/
std::vector<Polygon> PolygonFactory::getLeftToRightRect(const sf::Vector2f &pos, const sf::Vector2f &size, Material &material)
{
    sf::Vector2f p0(pos.x, pos.y);
    sf::Vector2f p1(pos.x + size.x, pos.y);
    sf::Vector2f p2(pos.x + size.x, pos.y + size.y);
    sf::Vector2f p3(pos.x, pos.y + size.y);

    return std::vector<Polygon> {
        Polygon( { p3, p0, p2 }, &material ),
        Polygon( { p0, p1, p2 }, &material )
    };
}

std::vector<Polygon> PolygonFactory::getRightToLeftRect(const sf::Vector2f &pos, const sf::Vector2f &size, Material &material)
{
    sf::Vector2f p0(pos.x, pos.y);
    sf::Vector2f p1(pos.x + size.x, pos.y);
    sf::Vector2f p2(pos.x + size.x, pos.y + size.y);
    sf::Vector2f p3(pos.x, pos.y + size.y);

    return std::vector<Polygon> {
        Polygon( { p3, p0, p1 }, &material ),
        Polygon( { p3, p1, p2 }, &material )
    };
}

/*
std::vector<Polygon> PolygonFactory::LoadInitialState()
{
    std::vector<Polygon> polygons;

    const sf::Vector2f offset(200, 100);
    const sf::Vector2f size(25, 25);

    for (int j = 0; j < 15 * 2; ++j)
    {
        for (int i = 0; i < 25 * 2; ++i)
        {
            std::vector<Polygon> rect;
            if (rand() % 2)
            {
                rect = GetLeftToRightRect(sf::Vector2f(offset.x + size.x * i, offset.y + size.y * j), size);
            }
            else
            {
                rect = GetRightToLeftRect(sf::Vector2f(offset.x + size.x * i, offset.y + size.y * j), size);
            }
            polygons.insert(polygons.end(), rect.begin(), rect.end());
        }
    }
    return polygons;
}


std::vector<Polygon> PolygonFactory::LoadInitialState()
{
    std::random_device r("awdawdawdawd");

    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, static_cast<int>(m_materials.size()) - 1);

    const sf::Vector2f tileSize(GlobalConst::DIM_WORLD_WIDTH / 160 * 2.0f, GlobalConst::DIM_WORLD_HEIGHT / 90 * 2.0f);

    std::vector<Polygon> polygons;
    sf::Vector2f pos(200, 200);

    for (int y = 0; y < 3; ++y)
    {
        for (int x = 0; x < 30; ++x)
        {
            std::vector<Polygon> rect;

            unsigned int material = static_cast<unsigned int>(uniform_dist(e1));

            if (rand() % 2)
                rect = getRightToLeftRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize, m_materials.at(material));
            else
                rect = getLeftToRightRect(sf::Vector2f(pos.x + tileSize.x * x, pos.y + tileSize.y * y), tileSize, m_materials.at(material));

            polygons.insert(polygons.end(), rect.begin(), rect.end());
        }
    }

    auto rect = getRightToLeftRect(sf::Vector2f(pos.x + tileSize.x * 10, pos.y + tileSize.y * 10), tileSize, m_materials.at(1));
    polygons.insert(polygons.end(), rect.begin(), rect.end());

    return polygons;
}

*/
