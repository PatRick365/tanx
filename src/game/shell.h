#pragma once

#include <SFML/Graphics.hpp>

class Shell
{
public:
    Shell(sf::Vector2f pos, float rot, unsigned int ownerId);

    void update(long long );

    bool attack(long long timeDiff, float damage);

    sf::Vector2f size() const;

    sf::Vector2f pos() const;

    std::vector<sf::Vector2f> polygon();

    float rot() const;

    float speed() const;
    void setSpeed(float speed);

    unsigned int ownerId() const;

private:
    sf::Vector2f m_size;
    sf::Vector2f m_pos;


    float m_speed;

    float m_rot;

    float m_health;

    unsigned int m_ownerId;


};

