#include "particle.h"
#include "../globalconst.h"

#include <cstdlib>
#include <ctime>
#include <iostream>

Particle::Particle(sf::Vector2f pos, Material::TextureId textureId) : m_pos(pos), m_textureId(textureId)
{
    m_lifeTime = 0;
    m_lifeTimeEnd = (rand() % 2000 + 1000) * 1000;;
    m_lifeTimeFinished = false;
    m_rot = static_cast<float>(rand() % 360);

    m_rotateLeft = rand() % 2;
}

void Particle::update(long long timeDiff)
{
    m_lifeTime += timeDiff;

    float timeFactor = timeDiff / GlobalConst::TANK_ACCELARATION * 1000;

    if (m_rotateLeft) timeFactor *= -1;

    m_rot += 0.3f * timeFactor;

    //float factor = m_lifeTime / static_cast<float>(m_lifeTimeEnd);

    if (m_lifeTime >= m_lifeTimeEnd) m_lifeTimeFinished = true;
}

bool Particle::lifeTimeFinished() const
{
    return m_lifeTimeFinished;
}

long long Particle::lifeTimeEnd() const
{
    return m_lifeTimeEnd;
}

long long Particle::lifeTime() const
{
    return m_lifeTime;
}

sf::Vector2f Particle::pos() const
{
    return m_pos;
}

float Particle::rot() const
{
    return m_rot;
}

Material::TextureId Particle::textureId() const
{
    return m_textureId;
}
