#pragma once

#include <SFML/Graphics.hpp>

class Explosion
{
public:
    Explosion(sf::Vector2f pos, unsigned int ownerId);

    void update(long long timeDiff);

    sf::Vector2f pos() const;

    float radius() const;
    float radiusEnd() const;

    float damage();

    bool isLifeTimeFinished() const;

    float scale() const;
    void setScale(float scale);

    unsigned int ownerId() const;

private:
    sf::Vector2f m_pos;

    float m_radius;
    float m_radiusEnd;

    float m_scale;

    float m_damage;

    long long m_lifeTime;
    long long m_lifeTimeEnd;
    bool m_lifeTimeFinished;

    unsigned int m_ownerId;

};
