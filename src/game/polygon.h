#pragma once

#include "material.h"

#include <SFML/Graphics.hpp>
#include <vector>

class Polygon
{
public:


    Polygon(std::vector<sf::Vector2f> m_points, Material *material);

    //Polygon();


    void setIsVisible(bool isVisible);
    bool isVisible() const;

    bool attack(long long timeDiff, float damage);

    const Material *material() const;

    const std::vector<sf::Vector2f> &points() const;

private:
    std::vector<sf::Vector2f> m_points;
    Material *m_material;

    bool m_isVisible;
    float m_health;







};

