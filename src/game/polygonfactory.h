#pragma once

#include "polygon.h"
#include "material.h"

#include <SFML/Graphics.hpp>
#include <vector>

class PolygonFactory
{
private:
    enum Side
    {
        DownRight,
        DownlLeft,
        UpRight,
        UpLeft
    };


public:
    PolygonFactory();

    std::vector<Polygon> LoadInitialState();


private:
    std::vector<Polygon> getWall(const sf::Vector2f &pos, const sf::Vector2f &tileSize, unsigned int width, unsigned int heigth, bool roundEdges = false);

    void setBlock(std::vector<Polygon> &polygons, int x, int y, Material::TextureId material);

    void setBunker(std::vector<Polygon> &polygons, int x, int y, const std::vector<Side> &doorSides);

    void setRect(std::vector<Polygon> &polygons, int x, int y, int size, Material::TextureId material, std::vector<Side> doorSides);

    void setHouse(std::vector<Polygon> &polygons, int x, int y, int size, int doorSide = -1);

    std::vector<Polygon> getLeftToRightRect(const sf::Vector2f &pos, const sf::Vector2f &size, Material &material);
    std::vector<Polygon> getRightToLeftRect(const sf::Vector2f &pos, const sf::Vector2f &size, Material &material);


private:
    std::vector<Material> m_materials;

};

