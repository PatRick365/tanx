#include "game.h"
#include "functions.h"
#include "../application.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <functional>
#include <algorithm>
#include <cmath>

using namespace std::chrono;

Game::Game(sf::RenderWindow &window, Settings &settings, SoundManager &soundManager, int numPlayers, int numPlayersHuman)
    : m_window(window), m_soundManager(soundManager), m_settings(settings), m_numPlayersHuman(numPlayersHuman)
{
    m_spawnPoints = { sf::Vector2f(static_cast<float>(GlobalConst::DIM_TANK_BODY_WIDTH) * 4,
                         static_cast<float>(GlobalConst::DIM_TANK_BODY_HEIGHT)  * 4),
                         sf::Vector2f(static_cast<float>(GlobalConst::DIM_WORLD_WIDTH) - GlobalConst::DIM_TANK_BODY_WIDTH  * 4,
                         static_cast<float>(GlobalConst::DIM_TANK_BODY_HEIGHT)  * 4),
                         sf::Vector2f(static_cast<float>(GlobalConst::DIM_WORLD_WIDTH) - GlobalConst::DIM_TANK_BODY_WIDTH  * 4,
                         static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT) - GlobalConst::DIM_TANK_BODY_HEIGHT  * 4),
                         sf::Vector2f(static_cast<float>(GlobalConst::DIM_TANK_BODY_WIDTH)  * 4,
                         static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT) - GlobalConst::DIM_TANK_BODY_HEIGHT  * 4) };

    // instantiate tanks, assign ais to non humans
    for (unsigned int i = 0; i < static_cast<unsigned int>(numPlayers); ++i)
    {
        m_tanks.emplace_back(Tank(i));

        if (i >= static_cast<unsigned int>(numPlayersHuman))
            m_ais.emplace_back(AiTank(*this, i));
    }

    //std::cout << "tanks: " << m_tanks.size() << ", ais: " << m_ais.size() << "\n";

    for (auto &tank : m_tanks)
        respawnTank(tank);


    m_polygons = m_polygonFactory.LoadInitialState();

    m_timestamp = time_point_cast<microseconds>(system_clock::now());

    m_stopGameFlag = false;

    m_frameTime = 0;
}

void Game::update()
{
    sf::Event event;

    m_releasedKeys.clear();

    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            m_window.close();

        if (event.type == sf::Event::KeyPressed)
        {
            m_pressedKeys.insert(event.key.code);
        }
        else if (event.type == sf::Event::KeyReleased)
        {
            m_releasedKeys.insert(event.key.code);
            m_pressedKeys.erase(event.key.code);
        }
    }

    if (m_pressedKeys.count(sf::Keyboard::Escape))
    {
        m_stopGameFlag = true;
        return;
    }

    // tanks control
    for (unsigned int i = 0; i < m_tanks.size(); ++i)
    {
        if (static_cast<int>(i) >= m_numPlayersHuman) break;
        if (m_pressedKeys.count(m_settings.keyUp(i)) ^ m_pressedKeys.count(m_settings.keyDown(i)))
        {
            if (m_pressedKeys.count(m_settings.keyUp(i)))
                m_tanks.at(i).setInputMove(1);
            else
                m_tanks.at(i).setInputMove(-1);
        }
        if (m_pressedKeys.count(m_settings.keyLeft(i)) ^ m_pressedKeys.count(m_settings.keyRight(i)))
        {
            if (m_pressedKeys.count(m_settings.keyLeft(i)))
                m_tanks.at(i).setInputRotate(-1);
            else
                m_tanks.at(i).setInputRotate(1);
        }
        if (m_pressedKeys.count(m_settings.keyAimLeft(i)) ^ m_pressedKeys.count(m_settings.keyAimRight(i)))
        {

            if (m_pressedKeys.count(m_settings.keyAimLeft(i)))
                m_tanks.at(i).setInputTurretRotate(1);
            else
                m_tanks.at(i).setInputTurretRotate(-1);
        }
        m_tanks.at(i).setInputShoot(m_pressedKeys.count(m_settings.keyShoot(i)));
    }

    if (m_releasedKeys.count(m_settings.keyRes0()))
        m_settings.setResolution(sf::Vector2u(1024, 576));
    else if (m_releasedKeys.count(m_settings.keyRes1()))
        m_settings.setResolution(sf::Vector2u(1152, 648));
    else if (m_releasedKeys.count(m_settings.keyRes2()))
        m_settings.setResolution(sf::Vector2u(1280, 720));
    else if (m_releasedKeys.count(m_settings.keyRes3()))
        m_settings.setResolution(sf::Vector2u(1366, 768));
    else if (m_releasedKeys.count(m_settings.keyRes4()))
        m_settings.setResolution(sf::Vector2u(1600, 900));
    else if (m_releasedKeys.count(m_settings.keyRes5()))
        m_settings.setResolution(sf::Vector2u(1920, 1080));
    if (m_releasedKeys.count(m_settings.keyFullscreen()))
        m_settings.setIsFullscreen(!m_settings.isFullscreen());

    std::chrono::microseconds elapsed = time_point_cast<microseconds>(system_clock::now()) - m_timestamp;
    m_timestamp = time_point_cast<microseconds>(system_clock::now());

    // update ais
    for (auto &ai : m_ais)
    {
        ai.update(elapsed.count());
    }

    // create shells
    for (auto &tank : m_tanks)
    {
        if (tank.wantToShoot())
        {
            tank.setBlowBack(true);
            m_shells.emplace_back(Shell(Functions::moveAngular(tank.pos(), tank.turretRot(), tank.turretSize().y / 2 + 25), tank.turretRot(), tank.id()));
            spawnParticles(m_shells.back().pos(), 5);
            m_soundManager.playSoundShot();
        }
    }

    bool tntExploded = false;

// tank collisions
    for (auto &tank : m_tanks)
    {
        for (auto &polygon : m_polygons)
        {
            if (!polygon.isVisible()) continue;

            bool bodyCollision = Functions::isPolygonIntersecting(tank.polygon(), polygon.points());
            bool turretCollision = Functions::isPolygonIntersecting(tank.polygonTurret(), polygon.points());

            if (bodyCollision || turretCollision)
            {
                float damage = std::abs(tank.speed()) * 1500;
                if (std::abs(tank.speed()) < 0.1f) damage = 0;
                bool polygonDestroyed = polygon.attack(elapsed.count(), damage);

                if (!polygonDestroyed)
                {
                    if (turretCollision) tank.restoreTurretPosition();  // correct ???
                    tank.restorePosition();
                    //if(tank.id() == 0 && (bodyCollision ^ turretCollision)) m_ai->setHitWall(true);
                    //if(tank.id() == 0) m_ai->setHitWall(true);
                    for (auto &ai : m_ais)
                        if (ai.tankId() == tank.id()) ai.setHitWall(true);
                }
                else
                {
                    spawnParticles(polygon.points().at(0), 1, polygon.material()->textureId());
                    tank.setSpeed(tank.speed() / 10 * 9);
                    if (polygon.material()->textureId() == Material::Tnt)
                    {
                        m_explosions.emplace_back(Explosion(polygon.points().at(0), tank.id()));
                        tntExploded = true;
                    }
                }
            }
        }
        for (auto &otherTank : m_tanks)
        {
            if (tank.id() == otherTank.id()) continue;
            if (Functions::isPolygonIntersecting(tank.polygon(), otherTank.polygon())
                    || Functions::isPolygonIntersecting(tank.polygon(), otherTank.polygonTurret())
                    || Functions::isPolygonIntersecting(tank.polygonTurret(), otherTank.polygonTurret()))
            {
                tank.restoreTurretPosition();
                tank.restorePosition();
                otherTank.restoreTurretPosition();
                otherTank.restorePosition();
            }
        }
        if (isOutOfBounds(tank.polygon()) ||isOutOfBounds(tank.polygonTurret()))
        {
            tank.restoreTurretPosition();
            tank.restorePosition();
            for (auto &ai : m_ais)
                if (ai.tankId() == tank.id()) ai.setHitWall(true);
        }
    }
// end tank collisions

// shell collisions
    for (int j = static_cast<int>(m_shells.size()); j --> 0; )  // prob fine with unsiged. or not. check if 0.
    {
        bool hit = false;
        for (auto &tank : m_tanks)
        {
            if (Functions::isPolygonIntersecting(m_shells.at(static_cast<unsigned int>(j)).polygon(), tank.polygon())
                    || Functions::isPolygonIntersecting(m_shells.at(static_cast<unsigned int>(j)).polygon(), tank.polygonTurret()))
            {
                hit = true;

                m_explosions.emplace_back(Explosion(tank.pos(), m_shells.at(static_cast<unsigned int>(j)).ownerId()));
                spawnParticles(tank.pos(), 5);
                //m_explosions.back().setScale(2);

                //std::cout << "shell owner: " << tankById(m_shells.at(static_cast<unsigned int>(j)).ownerId())->id() << ", tank hit: " << tank.id() << "\n";

                // why not other way around if/else ???
                if (tankById(m_shells.at(static_cast<unsigned int>(j)).ownerId() == tank.id()))
                    tankById(m_shells.at(static_cast<unsigned int>(j)).ownerId())->incrementPoints();
                else
                    tankById(m_shells.at(static_cast<unsigned int>(j)).ownerId())->decrementPoints();

                respawnTank(tank);
            }
        }
        if (isOutOfBounds(m_shells.at(static_cast<unsigned int>(j)).polygon()))
        {
            hit = true;
        }

        for (auto &polygon : m_polygons)
        {
            if (!polygon.isVisible()) continue;
            if (Functions::isPolygonIntersecting(m_shells.at(static_cast<unsigned int>(j)).polygon(), polygon.points()))
            {
                bool polygonDestroyed = polygon.attack(elapsed.count(), 300);
                if (!polygonDestroyed)
                {
                    hit = true;
                }
                else
                {
                    spawnParticles(polygon.points().at(0), 1, polygon.material()->textureId());
                    if (polygon.material()->textureId() == Material::Tnt)
                    {
                        m_explosions.emplace_back(Explosion(polygon.points().at(0), m_shells.at(static_cast<unsigned int>(j)).ownerId()));
                        tntExploded = true;
                    }
                    bool shellDestroyed = m_shells.at(static_cast<unsigned int>(j)).attack(elapsed.count(), polygon.material()->blastResistance() / 100);
                    if (shellDestroyed)
                    {
                        hit = true;
                    }
                    //m_shells.at(static_cast<unsigned int>(j)).setSpeed(m_shells.at(static_cast<unsigned int>(j)).speed() / 20 * 19);
                }
            }
        }

        if (hit)
        {
            spawnParticles(m_shells.at(static_cast<unsigned int>(j)).pos());
            m_explosions.emplace_back(Explosion(m_shells.at(static_cast<unsigned int>(j)).pos(), m_shells.at(static_cast<unsigned int>(j)).ownerId()));
            m_shells.erase(m_shells.begin() + j);
            m_soundManager.playSoundExplosion();
        }
    }
// end shell collisions

// explosion collisions
    for (auto &explosion : m_explosions)
    {
        Tank *tank = isCollidedTank(explosion.pos(), explosion.radius());
        if (tank != nullptr)
        {
            if (tank->id() == explosion.ownerId())
                tank->decrementPoints();
            else
                tankById(explosion.ownerId())->incrementPoints();

            respawnTank(*tank);
        }

        auto polysHit = isCollidedPolygon(explosion.pos(), explosion.radius());

        for (auto &hit : polysHit)
        {
            hit->attack(elapsed.count(), explosion.damage());
            if (!hit->isVisible())
            {
                if (hit->material()->textureId() == Material::Tnt)
                {
                    m_explosions.emplace_back(Explosion(hit->points().at(0), explosion.ownerId()));
                    tntExploded = true;
                }
            }
        }
    }
// end explosion collisions


    m_benchMarker.update(elapsed.count());

    if (tntExploded) m_soundManager.playSoundExplosion();

    // update tanks
    for (auto &tank : m_tanks)
    {
        tank.update(elapsed.count());
        //if (!tank.playedReloadSound())
        //    m_soundManager.playSoundReload();
    }
    // update shells
    for (auto &shell : m_shells)
    {
        shell.update(elapsed.count());
    }
    // update shells
    for (auto &explosion : m_explosions)
    {
        explosion.update(elapsed.count());
    }

    for (int j = static_cast<int>(m_particles.size()); j --> 0; )  // prob fine with unsiged. or not. check if 0.
    {
        m_particles.at(static_cast<unsigned int>(j)).update(elapsed.count());
        if (m_particles.at(static_cast<unsigned int>(j)).lifeTimeFinished())
            m_particles.erase(m_particles.begin() + j);
    }


    // update shells
    for (auto &particle : m_particles)
    {
        particle.update(elapsed.count());
    }

    for (int i = static_cast<int>(m_explosions.size()); i --> 0; )
    {
        m_explosions[static_cast<unsigned int>(i)].update(elapsed.count());
        if (m_explosions[static_cast<unsigned int>(i)].isLifeTimeFinished())
        {
            m_explosions.erase(m_explosions.begin() + i);
        }
    }
}

int Game::isCollidedTank(const std::vector<sf::Vector2f> &polygon)
{
    int collisions = 0;
    for (auto &tankyMcTankFace : m_tanks)
    {
        if (Functions::isPolygonIntersecting(tankyMcTankFace.polygon(), polygon)) collisions++;
        if (Functions::isPolygonIntersecting(tankyMcTankFace.polygonTurret(), polygon)) collisions++;
    }
    return collisions;
}

Tank *Game::isCollidedTank(sf::Vector2f pos, float radius)
{
    for (auto &tank : m_tanks)
    {
        for (const auto &poly : tank.polygon())
        {
            if (std::pow((poly.x - pos.x), 2) + std::pow((poly.y - pos.y), 2) < std::pow(radius, 2))
                return &tank;
        }
    }
    return nullptr;
}

std::vector<Polygon *> Game::isCollidedPolygon(sf::Vector2f pos, float radius)
{
    std::vector<Polygon *> colPolys;

    for (auto &polygon : m_polygons)
    {
        if (!polygon.isVisible()) continue;

        for (const auto &point : polygon.points())
        {
            if (std::pow((point.x - pos.x), 2) + std::pow((point.y - pos.y), 2) < std::pow(radius, 2))
                colPolys.push_back(&polygon);
        }
    }
    return colPolys;
}

bool Game::isCollidedPolygon(const std::vector<sf::Vector2f> &polygon)
{
    for (auto &poly : m_polygons)
    {
        if (Functions::isPolygonIntersecting(poly.points(), polygon)) return true;
    }
    return false;
}

bool Game::isOutOfBounds(std::vector<sf::Vector2f> polygon)
{
    for (const auto &point : polygon)
    {
        if (point.x < 0) return true;
        //if (point.x > GlobalConst::WORLD_TILES_WIDTH * GlobalConst::DIM_TILES_WIDTH) return true;
        if (point.x > GlobalConst::DIM_WORLD_WIDTH) return true;
        if (point.y < 0) return true;
        //if (point.y > GlobalConst::WORLD_TILES_HEIGHT * GlobalConst::DIM_TILES_HEIGHT) return true;
        if (point.y > GlobalConst::DIM_WORLD_HEIGHT) return true;
    }
    return false;
}

void Game::respawnTank(Tank &tank)
{
    //float avDist;

    //unsigned int farthestPointIndex = 0;
    //if (tank.lastSpawnIndex() == 0) farthestPointIndex++;

    //std::vector<float> avDist;
    //avDist.reserve(m_spawnPoints.size());

    //std::map<unsigned int, float> map;

    std::vector<std::pair<unsigned int, float>> avDist;
    avDist.reserve(m_spawnPoints.size());

    for (unsigned int i = 0; i < m_spawnPoints.size(); ++i)
    {
        float distSum = 0;
        float smallest = -1;
        for (const auto &otherTank : m_tanks)
        {
            distSum += std::abs(Functions::distanceBetween(m_spawnPoints.at(i), otherTank.pos()));
            float dist = std::abs(Functions::distanceBetween(m_spawnPoints.at(i), otherTank.pos()));
            if (smallest < 0 || dist < smallest)
            {
                smallest = dist;
            }
        }
        //avDist.push_back(distSum / m_spawnPoints.size());
        //map.insert( {i, distSum / m_spawnPoints.size()} );
        //avDist.push_back({i, distSum / m_spawnPoints.size()});
        avDist.emplace_back(std::pair<int, float>{i, smallest / m_spawnPoints.size()});

    }

    std::sort(avDist.begin(), avDist.end(), [](auto &left, auto &right) {
        return left.second > right.second;
    });

    for (const auto &dist : avDist)
    {
        if (static_cast<int>(dist.first) != tank.lastSpawnIndex())
        {
            tank.setLastSpawnIndex(static_cast<int>(dist.first));
            tank.setPos(m_spawnPoints.at(dist.first));
            tank.reset(90.0f * (dist.first + 1));
            return;
        }
    }


    /*

    for (unsigned int i = 0; i < m_spawnPoints.size(); ++i)
    {

        for (const auto &otherTank : m_tanks)
        {
            avDist = Functions::distanceBetween(tank.pos(), otherTank.pos());
        }
        avDist /= m_tanks.size();

        //if (avDist > Functions::distanceBetween(m_spawnPoints.at(farthestPointIndex), otherTank.pos()))

    }

        unsigned int farthestPointIndex = 0;
        if (tank.lastSpawnIndex() == 0) farthestPointIndex++;
        for (unsigned int i = 0; i < m_spawnPoints.size(); ++i)
        {
            //std::cout << "i: " << i << ", " << distanceBetween(m_spawnPoints.at(i), otherTank.pos()) << "\n";
            if (Functions::distanceBetween(m_spawnPoints.at(i), otherTank.pos()) > Functions::distanceBetween(m_spawnPoints.at(farthestPointIndex), otherTank.pos()))
            {
                if (tank.lastSpawnIndex() != static_cast<int>(i))
                    farthestPointIndex = i;
            }
        }
        tank.setLastSpawnIndex(static_cast<int>(farthestPointIndex));
        tank.setPos(m_spawnPoints.at(farthestPointIndex));
        tank.reset(90.0f * (farthestPointIndex + 1));
    }
*/

    /*for (const auto &otherTank : m_tanks)
    {
        if (otherTank.id() == tank.id()) continue;

        unsigned int farthestPointIndex = 0;
        if (tank.lastSpawnIndex() == 0) farthestPointIndex++;
        for (unsigned int i = 0; i < m_spawnPoints.size(); ++i)
        {
            //std::cout << "i: " << i << ", " << distanceBetween(m_spawnPoints.at(i), otherTank.pos()) << "\n";
            if (Functions::distanceBetween(m_spawnPoints.at(i), otherTank.pos()) > Functions::distanceBetween(m_spawnPoints.at(farthestPointIndex), otherTank.pos()))
            {
                if (tank.lastSpawnIndex() != static_cast<int>(i))
                    farthestPointIndex = i;
            }
        }
        tank.setLastSpawnIndex(static_cast<int>(farthestPointIndex));
        tank.setPos(m_spawnPoints.at(farthestPointIndex));
        tank.reset(90.0f * (farthestPointIndex + 1));
        return;
    }*/
}

void Game::spawnParticles(sf::Vector2f pos, unsigned int multiplier, Material::TextureId textureId)
{
    for (unsigned int i = 0; i < multiplier; ++i)
    {
        unsigned int numParticles = rand() % 2 + 2;
        for (unsigned int i = 0; i < numParticles; ++i)
        {
            int xOffset = 15 - rand() % 30;
            int yOffset = 15 - rand() % 30;
            m_particles.emplace_back(Particle(sf::Vector2f(pos.x + xOffset, pos.y + yOffset), textureId));
        }
    }
    //std::cout << "numParticles: " << m_particles.size() << "\n";
}

Tank *Game::tankById(unsigned int id)
{
    for (Tank &tank : m_tanks)
    {
        if (tank.id() == id) return &tank;
    }
    return nullptr;
}

bool Game::stopGameFlag() const
{
    return m_stopGameFlag;
}

std::vector<Polygon> &Game::polygons()
{
    return m_polygons;
}

std::vector<Shell> &Game::shells()
{
    return m_shells;
}

std::vector<Tank> &Game::tanks()
{
    return m_tanks;
}

std::vector<Explosion> &Game::explosions()
{
    return m_explosions;
}

std::vector<Particle> &Game::particles()
{
    return m_particles;
}

const BenchMarker &Game::benchMarker() const
{
    return m_benchMarker;
}
