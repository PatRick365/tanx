#include "application.h"

int main()
{
    Application app;

    int exitcode = app.run();

    return exitcode;
}
