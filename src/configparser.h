#pragma once

#include <map>
#include <string>

class ConfigParser
{
public:
    //ConfigParser();

    static std::map<std::string, std::string> loadConfig();

    static void saveConfig(std::map<std::string, std::string> map);

    static bool doesConfigExist();


private:
    static std::pair<std::string, std::string> parseLine(std::string line);


};
