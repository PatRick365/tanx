#include "soundmanager.h"

#include <string>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include <iostream>

using namespace std::chrono;

SoundManager::SoundManager(Settings &settings) : m_settings(settings)
{
    srand(static_cast<unsigned int>(time(nullptr)));

    m_buffersShot.resize(1);
    if (!m_buffersShot[0].loadFromFile("resources/sounds/tank_firing1.wav"))
        return;

    m_buffersReload.resize(1);
    if (!m_buffersReload[0].loadFromFile("resources/sounds/tank_reload.wav"))
        return;

    m_buffersExplosion.resize(3);
    if (!m_buffersExplosion[0].loadFromFile("resources/sounds/explosion5.wav"))
        return;
    if (!m_buffersExplosion[1].loadFromFile("resources/sounds/explosion7.wav"))
        return;
    if (!m_buffersExplosion[2].loadFromFile("resources/sounds/explosion8.wav"))
        return;


    /*m_buffersExplosion.resize(9);
    for (unsigned int i = 1; i <= 9; ++i)
    {
        std::cout << i << "\n";
        if (!m_buffersExplosion[i - 1].loadFromFile("resources/sounds/explosion" + std::to_string(i) + ".wav"))
            return;
    }*/

    m_timestampExplosion = time_point_cast<milliseconds>(system_clock::now());

}

void SoundManager::update()
{
    // remove sounds that stopped playing
    for (auto it = m_sounds.begin(); it != m_sounds.end(); )
    {
        bool isActive = ((*it).getStatus() != sf::Sound::Stopped);
        if (!isActive)
        {
            m_sounds.erase(it++);  // alternatively, i = items.erase(i);
        }
        else
        {
            ++it;
        }
    }
}

void SoundManager::queueSound(const sf::Sound &sound)
{
    m_sounds.push_back(sound);
    m_sounds.back().setVolume(static_cast<float>(m_settings.volumeSound()));
    m_sounds.back().play();
}

void SoundManager::playSoundShot()
{
    unsigned int randomIndex = static_cast<unsigned int>(rand()) % m_buffersShot.size();
    queueSound(sf::Sound(m_buffersShot.at(randomIndex)));
}

void SoundManager::playSoundReload()
{
    unsigned int randomIndex = static_cast<unsigned int>(rand()) % m_buffersReload.size();
    queueSound(sf::Sound(m_buffersReload.at(randomIndex)));
}

void SoundManager::playSoundExplosion()
{
    /*static int i = 0;
    unsigned int randomIndex = static_cast<unsigned int>(rand()) % m_buffersExplosion.size();
    randomIndex = i++;
    if (i >= m_buffersExplosion.size() - 1) i = 0;*/

    std::chrono::milliseconds elapsed = time_point_cast<milliseconds>(system_clock::now()) - m_timestampExplosion;

    if (elapsed.count() > 100)
    {
        unsigned int randomIndex = static_cast<unsigned int>(rand()) % m_buffersExplosion.size();
        queueSound(sf::Sound(m_buffersExplosion.at(randomIndex)));
        m_timestampExplosion = time_point_cast<milliseconds>(system_clock::now());
    }
}


