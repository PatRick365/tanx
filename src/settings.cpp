#include "settings.h"
#include "configparser.h"

#include <map>
#include <string>
#include <iostream>

Settings::Settings()
{
    // player 1 controls
    m_keyUp.push_back(sf::Keyboard::Key::Up);
    m_keyDown.push_back(sf::Keyboard::Key::Down);
    m_keyLeft.push_back(sf::Keyboard::Key::Left);
    m_keyRight.push_back(sf::Keyboard::Key::Right);
    m_keyShoot.push_back(sf::Keyboard::Key::Period);
    m_keyAimLeft.push_back(sf::Keyboard::Key::Comma);
    m_keyAimRight.push_back(sf::Keyboard::Key::Hyphen);

    // player 2 controls
    m_keyUp.push_back(sf::Keyboard::Key::W);
    m_keyDown.push_back(sf::Keyboard::Key::S);
    m_keyLeft.push_back(sf::Keyboard::Key::A);
    m_keyRight.push_back(sf::Keyboard::Key::D);
    m_keyShoot.push_back(sf::Keyboard::Key::G);
    m_keyAimLeft.push_back(sf::Keyboard::Key::F);
    m_keyAimRight.push_back(sf::Keyboard::Key::H);

    m_keyRes0 = sf::Keyboard::Key::F1;
    m_keyRes1 = sf::Keyboard::Key::F2;
    m_keyRes2 = sf::Keyboard::Key::F3;
    m_keyRes3 = sf::Keyboard::Key::F4;
    m_keyRes4 = sf::Keyboard::Key::F5;
    m_keyRes5 = sf::Keyboard::Key::F6;
    m_keyFullscreen = sf::Keyboard::Key::F11;

    m_resolution = availableResolutions().back();
    m_isFullscreen = false;
    m_volumeSound = 20;

    m_videoChangedFlag = false;

    m_showFps = false;

    m_players = 4;
    m_playersHuman = 1;

    if (!ConfigParser::doesConfigExist())
    {
        saveToConfig();
    }
    else
    {
        loadFromConfig();
    }
}

std::vector<sf::Vector2u> Settings::availableResolutions() const
{
    std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();

    std::vector<sf::Vector2u> resolutions;
    for (auto it = modes.rbegin(); it != modes.rend(); ++it)
    {
        if (it->bitsPerPixel == 32)
            resolutions.emplace_back(sf::Vector2u(it->width, it->height));
    }
    return resolutions;
}

void Settings::saveToConfig()
{
    std::map<std::string, std::string> map;

    map.insert({"res_x", std::to_string(m_resolution.x)});
    map.insert({"res_y", std::to_string(m_resolution.y)});
    map.insert({"vol_sound", std::to_string(m_volumeSound)});
    map.insert({"fullscrn", std::to_string(m_isFullscreen)});
    map.insert({"show_fps", std::to_string(m_showFps)});

    ConfigParser::saveConfig(map);
}

void Settings::loadFromConfig()
{
    std::map<std::string, std::string> map = ConfigParser::loadConfig();

    bool fileInvalid = false;

    if (map.find("res_x") != map.end())
        m_resolution.x = static_cast<unsigned int>(std::stoi(map.find("res_x")->second));
    else fileInvalid = true;
    if (map.find("res_y") != map.end())
        m_resolution.y = static_cast<unsigned int>(std::stoi(map.find("res_y")->second));
    else fileInvalid = true;
    if (map.find("vol_sound") != map.end())
        m_volumeSound = std::stoi(map.find("vol_sound")->second);
    else fileInvalid = true;
    if (map.find("fullscrn") != map.end())
        m_isFullscreen = std::stoi(map.find("fullscrn")->second);
    else fileInvalid = true;
    if (map.find("show_fps") != map.end())
        m_showFps = std::stoi(map.find("show_fps")->second);
    else fileInvalid = true;

    if (fileInvalid)
    {
        std::cout << "config file invalid. fixing it.\n";
        saveToConfig();
    }
}

bool Settings::showFps() const
{
    return m_showFps;
}

void Settings::setShowFps(bool showFps)
{
    m_showFps = showFps;
}

int Settings::playersHuman() const
{
    return m_playersHuman;
}

void Settings::setPlayersHuman(int playersHuman)
{
    m_playersHuman = playersHuman;
}

int Settings::players() const
{
    return m_players;
}

void Settings::setPlayers(int players)
{
    m_players = players;
}

sf::Keyboard::Key Settings::keyUp(unsigned int player)
{
    if (player >= m_keyUp.size()) return sf::Keyboard::Unknown;
    return m_keyUp.at(player);
}
sf::Keyboard::Key Settings::keyDown(unsigned int player)
{
    if (player >= m_keyDown.size()) return sf::Keyboard::Unknown;
    return m_keyDown.at(player);
}
sf::Keyboard::Key Settings::keyLeft(unsigned int player)
{
    if (player >= m_keyLeft.size()) return sf::Keyboard::Unknown;
    return m_keyLeft.at(player);
}
sf::Keyboard::Key Settings::keyRight(unsigned int player)
{
    if (player >= m_keyRight.size()) return sf::Keyboard::Unknown;
    return m_keyRight.at(player);
}

sf::Keyboard::Key Settings::keyShoot(unsigned int player)
{
    if (player >= m_keyShoot.size()) return sf::Keyboard::Unknown;
    return m_keyShoot.at(player);
}
sf::Keyboard::Key Settings::keyAimLeft(unsigned int player)
{
    if (player >= m_keyAimLeft.size()) return sf::Keyboard::Unknown;
    return m_keyAimLeft.at(player);
}
sf::Keyboard::Key Settings::keyAimRight(unsigned int player)
{
    if (player >= m_keyAimRight.size()) return sf::Keyboard::Unknown;
    return m_keyAimRight.at(player);
}

sf::Vector2u Settings::resolution() const
{
    return m_resolution;
}

void Settings::setResolution(const sf::Vector2u &resolution)
{
    if (resolution != m_resolution)
    {
        m_videoChangedFlag = true;
        m_resolution = resolution;
    }
}

sf::Keyboard::Key Settings::keyRes0() const
{
    return m_keyRes0;
}

sf::Keyboard::Key Settings::keyRes1() const
{
    return m_keyRes1;
}

sf::Keyboard::Key Settings::keyRes2() const
{
    return m_keyRes2;
}

sf::Keyboard::Key Settings::keyRes3() const
{
    return m_keyRes3;
}

bool Settings::videoChangedFlag()
{
    if (m_videoChangedFlag)
    {
        m_videoChangedFlag = false;
        return true;
    }
    return false;
}

bool Settings::isFullscreen() const
{
    return m_isFullscreen;
}

void Settings::setIsFullscreen(bool isFullscreen)
{
    if (isFullscreen != m_isFullscreen)
    {
        m_videoChangedFlag = true;
        m_isFullscreen = isFullscreen;
    }
}

int Settings::volumeSound() const
{
    return m_volumeSound;
}

void Settings::setVolumeSound(int volumeSound)
{
    m_volumeSound = volumeSound;
}

sf::Keyboard::Key Settings::keyFullscreen() const
{
    return m_keyFullscreen;
}

sf::Keyboard::Key Settings::keyRes4() const
{
    return m_keyRes4;
}

sf::Keyboard::Key Settings::keyRes5() const
{
    return m_keyRes5;
}
