#include "benchmarker.h"

using namespace std::chrono;

BenchMarker::BenchMarker()
{
    m_timeDiff = 0;
    m_frameCounter = 0;
    m_fps = 0;

    m_timeDiffSum = 0;

    //m_timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
    m_timestamp = time_point_cast<milliseconds>(system_clock::now());

}

void BenchMarker::update(long long timeDiff)
{
    m_frameCounter++;
    //std::chrono::milliseconds elapsed = time_point_cast<milliseconds>(system_clock::now()).time_since_epoch() - m_timestamp;
    std::chrono::milliseconds elapsed = time_point_cast<milliseconds>(system_clock::now()) - m_timestamp;

    m_timeDiffSum+= timeDiff;

    if (elapsed.count() >= 1000)
    {
        m_fps = m_frameCounter;
        m_frameCounter = 0;
        //m_timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
        m_timestamp = time_point_cast<milliseconds>(system_clock::now());

        m_timeDiff = m_timeDiffSum / m_fps;
    }

    //m_timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());


}

unsigned int BenchMarker::fps() const
{
    return m_fps;
}

float BenchMarker::timeDiff() const
{
    return m_timeDiff;
}

/*
 *
 *     std::chrono::milliseconds elapsed;

    do
    {
        elapsed = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch()) - m_timestamp;
        m_timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
    }
*/
