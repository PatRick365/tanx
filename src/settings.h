#pragma once

#include <SFML/Graphics.hpp>
#include "vector"

class Settings
{
public:
    Settings();

    std::vector<sf::Vector2u> availableResolutions() const;

    void saveToConfig();

    sf::Keyboard::Key keyUp(unsigned int player);
    sf::Keyboard::Key keyDown(unsigned int player);
    sf::Keyboard::Key keyLeft(unsigned int player);
    sf::Keyboard::Key keyRight(unsigned int player);

    sf::Keyboard::Key keyShoot(unsigned int player);
    sf::Keyboard::Key keyAimLeft(unsigned int player);
    sf::Keyboard::Key keyAimRight(unsigned int player);

    sf::Keyboard::Key keyRes0() const;
    sf::Keyboard::Key keyRes1() const;
    sf::Keyboard::Key keyRes2() const;
    sf::Keyboard::Key keyRes3() const;
    sf::Keyboard::Key keyRes4() const;
    sf::Keyboard::Key keyRes5() const;
    sf::Keyboard::Key keyFullscreen() const;

    sf::Vector2u resolution() const;
    void setResolution(const sf::Vector2u &resolution);

    bool videoChangedFlag();

    bool isFullscreen() const;
    void setIsFullscreen(bool isFullscreen);

    int volumeSound() const;
    void setVolumeSound(int volumeSound);

    int players() const;
    void setPlayers(int players);

    int playersHuman() const;
    void setPlayersHuman(int playersHuman);

    bool showFps() const;
    void setShowFps(bool showFps);

private:
    void loadFromConfig();


private:
    std::vector<sf::Keyboard::Key> m_keyUp;
    std::vector<sf::Keyboard::Key> m_keyDown;
    std::vector<sf::Keyboard::Key> m_keyLeft;
    std::vector<sf::Keyboard::Key> m_keyRight;

    std::vector<sf::Keyboard::Key> m_keyShoot;
    std::vector<sf::Keyboard::Key> m_keyAimLeft;
    std::vector<sf::Keyboard::Key> m_keyAimRight;

    sf::Keyboard::Key m_keyRes0;
    sf::Keyboard::Key m_keyRes1;
    sf::Keyboard::Key m_keyRes2;
    sf::Keyboard::Key m_keyRes3;
    sf::Keyboard::Key m_keyRes4;
    sf::Keyboard::Key m_keyRes5;
    sf::Keyboard::Key m_keyFullscreen;

    sf::Vector2u m_resolution;
    bool m_isFullscreen;
    bool m_showFps;

    bool m_videoChangedFlag;

    int m_volumeSound;

    int m_players;
    int m_playersHuman;

};

