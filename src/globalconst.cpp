#include "globalconst.h"

namespace GlobalConst
{

constexpr unsigned int DIM_WORLD_WIDTH = 1920;
constexpr unsigned int DIM_WORLD_HEIGHT = 1080;

constexpr unsigned int DIM_TANK_BODY_WIDTH = 36;
constexpr unsigned int DIM_TANK_BODY_HEIGHT = 60;

constexpr unsigned int DIM_TANK_TURRET_WIDTH = 15;
constexpr unsigned int DIM_TANK_TURRET_HEIGHT = 50;

constexpr unsigned int DIM_SHELL_WIDTH = 13;
constexpr unsigned int DIM_SHELL_HEIGHT = 13;

constexpr unsigned int DIM_TILES_WIDTH = DIM_WORLD_WIDTH / 160 * 2;
constexpr unsigned int DIM_TILES_HEIGHT = DIM_WORLD_HEIGHT / 90 * 2;

constexpr unsigned int WORLD_TILES_WIDTH = 81;
constexpr unsigned int WORLD_TILES_HEIGHT = 46;

constexpr float TANK_ACCELARATION = 2500000.0f;
constexpr float TANK_DECELARATION = 10.0f;

constexpr float SHELL_ACCELARATION = 1000.0f;

constexpr char CONFIG_FILE_NAME[] = "config";

}
