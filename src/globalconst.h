#pragma once

#include <string>

namespace GlobalConst
{

extern const unsigned int DIM_WORLD_WIDTH;
extern const unsigned int DIM_WORLD_HEIGHT;

extern const unsigned int DIM_TANK_BODY_WIDTH;
extern const unsigned int DIM_TANK_BODY_HEIGHT;

extern const unsigned int DIM_TANK_TURRET_WIDTH;
extern const unsigned int DIM_TANK_TURRET_HEIGHT;

extern const unsigned int DIM_SHELL_WIDTH;
extern const unsigned int DIM_SHELL_HEIGHT;

extern const unsigned int DIM_TILES_WIDTH;
extern const unsigned int DIM_TILES_HEIGHT;

extern const unsigned int WORLD_TILES_WIDTH;
extern const unsigned int WORLD_TILES_HEIGHT;

extern const float TANK_ACCELARATION;
extern const float TANK_DECELARATION;

extern const float SHELL_ACCELARATION;

extern const char CONFIG_FILE_NAME[];

}

