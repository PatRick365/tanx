#include "configparser.h"
#include "globalconst.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

//ConfigParser::ConfigParser() {}

std::map<std::string, std::string> ConfigParser::loadConfig()
{
    std::ifstream infile(GlobalConst::CONFIG_FILE_NAME);

    std::map<std::string, std::string> map;

    std::string line;
    while (std::getline(infile, line))
    {
        auto pair = parseLine(line);
        map.insert({ pair.first, pair.second });
    }
    return map;
}

void ConfigParser::saveConfig(std::map<std::string, std::string> map)
{
    std::ofstream out(GlobalConst::CONFIG_FILE_NAME);

    for (const auto &pair : map)
    {
        out << pair.first << "=" << pair.second << "\n";
    }
    out.close();
}

std::pair<std::string, std::string> ConfigParser::parseLine(std::string line)
{
    std::pair<std::string, std::string> pair;

    line.erase(remove_if(line.begin(), line.end(), isspace), line.end());

    std::size_t foundComment = line.find('#');
    if (foundComment != std::string::npos)
    {
        line.erase(foundComment, line.size() - foundComment);
    }

    //std::cout << line << "\n";

    std::size_t foundEquals = line.find('=');
    if (foundEquals == std::string::npos)
        return pair;

    pair.first = std::string(line.begin(), line.begin() + static_cast<int>(foundEquals));
    pair.second = std::string(line.begin() + static_cast<int>(foundEquals) + 1, line.end());

    //std::cout << "key: '" << key << "', value: '" << value << "'\n";

    return pair;
}

bool ConfigParser::doesConfigExist()
{
    std::ifstream file(GlobalConst::CONFIG_FILE_NAME);
    return file.good();
}
