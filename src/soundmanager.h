#pragma once

#include "globalconst.h"
#include "settings.h"

#include <SFML/Audio.hpp>
#include <vector>
#include <list>
#include <chrono>


class SoundManager
{
public:
    using time_stamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>;

    SoundManager(Settings &settings);

    void update();

    void playSoundShot();
    void playSoundReload();
    void playSoundExplosion();


private:
    void queueSound(const sf::Sound &sound);

private:
    Settings &m_settings;

    std::vector<sf::SoundBuffer> m_buffersShot;
    std::vector<sf::SoundBuffer> m_buffersReload;
    std::vector<sf::SoundBuffer> m_buffersExplosion;

    std::list<sf::Sound> m_sounds;

    time_stamp m_timestampExplosion;


};

