// Author:      Patrick Falkensteiner
// Description: Main class of project
//              Initializes sf::RenderWindow, and can recreate it if settings change
//              Holds application wide Settings object
//              Instantiates and holds MainMenu for menu gui
//              Instatntiates and holds SoundManager, which plays all sounds
//              Instantiates and holds Game, which is the games god class
//              Instatntiates and holds Renderer, which renders all ingame graphics
//              Controls whether the menu or the game is rendered

#pragma once

#include "game/gamerenderer.h"
#include "game/game.h"
#include "mainmenu/mainmenu.h"
#include "settings.h"
#include "soundmanager.h"

#include <SFML/Graphics.hpp>
#include <memory>

class Application
{
public:
    enum RunningState
    {
        MainMenuState,
        GameState
    };

    Application();
    ~Application();

    void createWindowFromSettings();

    int run();

    void initMainMenu();
    void initGame(int numPlayers, int numPlayersHuman);
    void stopGame();


private:
    sf::RenderWindow m_window;
    Settings m_settings;
    GameRenderer m_gameRenderer;
    SoundManager m_soundManager;

    MainMenu *m_mainMenu;

    std::unique_ptr<Game> m_game;

    RunningState m_runningState;

};

