// Author:      Patrick Falkensteiner
// Description: Main class of project
//              Initializes sf::RenderWindow, and can recreate it if settings change
//              Holds application wide Settings object
//              Instantiates and holds MainMenu for menu gui
//              Instatntiates and holds SoundManager, which plays all sounds
//              Instantiates and holds Game, which is the games god class
//              Instatntiates and holds Renderer, which renders all ingame graphics
//              Controls whether the menu or the game is rendered

#include "application.h"
#include "globalconst.h"

#include <iostream>

#define DEBUG_START_GAME_IMMEDIATELY false

Application::Application() : m_gameRenderer(m_window, m_settings), m_soundManager(m_settings)
{
    createWindowFromSettings();

    m_mainMenu = new MainMenu(m_window, m_settings, m_soundManager);

    m_runningState = RunningState::MainMenuState;
    if (DEBUG_START_GAME_IMMEDIATELY) m_runningState = RunningState::GameState;

    if (m_runningState == RunningState::MainMenuState)
    {
        initMainMenu();
    }
    else if (m_runningState == RunningState::GameState) // for debugging
    {
        initGame(1, 1);
    }
}

Application::~Application()
{
    delete m_mainMenu;
}

void Application::createWindowFromSettings()
{
    //sf::ContextSettings settings;
    //settings.antialiasingLevel = 4;
    //m_window.setVerticalSyncEnabled(true);

    m_window.create(sf::VideoMode(m_settings.resolution().x, m_settings.resolution().y), "Tanx", m_settings.isFullscreen() ? sf::Style::Fullscreen : sf::Style::Default/*, settings*/);
    m_window.setMouseCursorVisible(false);

    sf::Image ico;
    ico.loadFromFile("resources/icons/tank_ico.png");
    m_window.setIcon(ico.getSize().x, ico.getSize().y, ico.getPixelsPtr());


}

int Application::run()
{
    while (m_window.isOpen())
    {
        if (m_runningState == RunningState::MainMenuState)
        {
            m_mainMenu->update();
            if (m_mainMenu->queryStartGameFlag())
                initGame(m_settings.players(), m_settings.playersHuman());
        }
        else if (m_runningState == RunningState::GameState)
        {
            m_game->update();

            if (m_game->stopGameFlag())
            {
                //stopGame();
                m_game.reset();
                m_runningState = RunningState::MainMenuState;
                m_window.setFramerateLimit(30);
            }
            else
            {
                m_gameRenderer.update(*m_game);
            }
        }

        m_soundManager.update();

        if (m_settings.videoChangedFlag())
        {
            createWindowFromSettings();
        }
    }
    return 0;
}

void Application::initMainMenu()
{
    //m_renderer.reset(new Renderer(m_window, m_settings));
    //m_window.setFramerateLimit(30);
}

void Application::initGame(int numPlayers, int numPlayersHuman)
{
    //m_renderer.reset(new Renderer(m_window, m_settings));
    //m_world.reset(new World(m_window, m_settings, m_soundManager, numPlayers, numPlayersHuman));
    m_game = std::make_unique<Game>(m_window, m_settings, m_soundManager, numPlayers, numPlayersHuman);

    m_runningState = RunningState::GameState;
    m_window.setFramerateLimit(0);
}

void Application::stopGame()
{
    //m_world.release();
    m_game.reset();
    m_runningState = RunningState::MainMenuState;
    m_window.setFramerateLimit(30);
}

