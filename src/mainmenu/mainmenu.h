// Author:      Patrick Falkensteiner
// Description: Manages menuing system
//              Holds all menu forms
//              Handles input and passes it on to the active form
//              Performs actions according to gui-element interactions like altering the Settings object,
//                  navigate the gui or setting the start-game-flag
//              Renders active form

#pragma once

#include "../settings.h"
#include "form.h"
#include "../soundmanager.h"

#include <SFML/Graphics.hpp>
#include <vector>
#include <set>

class MainMenu
{
public:
    MainMenu(sf::RenderWindow &window, Settings &settings, SoundManager &soundManager);

    void update();

    bool queryStartGameFlag();

private:
    void handleInput();
    void render();

    void performAction(GuiElement::Action action);

    void focusForm(Form::Type formType);

    GuiElement &getSelectedGuiElement();

private:
    sf::RenderWindow &m_window;
    Settings &m_settings;
    SoundManager &m_soundManager;

    sf::Texture m_background;

    std::vector<Form> m_forms;

    std::set<sf::Keyboard::Key> m_pressedKeys;
    std::set<sf::Keyboard::Key> m_releasedKeys;

    bool m_startGameFlag;

};

