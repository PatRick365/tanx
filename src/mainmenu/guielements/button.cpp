#include "button.h"

Button::Button(int tabId, std::string text, Action action, std::string description)
    : GuiElement(GuiElement::Type::Button, tabId, action, std::move(description)), m_text(std::move(text))
{
}

std::string Button::text() const
{
    return m_text;
}

sf::Vector2f Button::pos() const
{
    return m_pos;
}

void Button::setPos(const sf::Vector2f &pos)
{
    m_pos = pos;
}

sf::Vector2f Button::size() const
{
    return m_size;
}

void Button::setSize(const sf::Vector2f &size)
{
    m_size = size;
}

