// Author:      Patrick Falkensteiner
// Description: Base class for all gui-elements
//              Holds Type enum that identifies the type of an element
//              Holds Action enum that identifies all possible actions that
//              can be performed by an element

#pragma once

#include <SFML/Graphics.hpp>

class GuiElement
{
public:
    enum Type
    {
        Button,
        NumberSelector,
        BoolSelector,
        ResolutionSelector
    };

    enum Action
    {
        ActionStartGame,
        ActionEnterSettings,
        ActionEnterMainMenu,
        ActionQuit,

        ActionEnterVideoSettings,
        ActionEnterAudioSettings,
        ActionEnterGameSettings,

        ActionChangeSoundVolume,

        ActionChangeFullScreen,
        ActionChangeResolution,
        ActionChangeShowFps,

        ActionChangePlayers,
        ActionChangePlayersHuman,

        ActionNone
    };


public:
    GuiElement(Type type, int tabId, Action action, std::string description);

    virtual ~GuiElement() = default;

    Type type() const;
    int tabId() const;
    Action action() const;

    std::string description() const;

private:
    Type m_type;
    int m_tabId;
    Action m_action;
    std::string m_description;

};

