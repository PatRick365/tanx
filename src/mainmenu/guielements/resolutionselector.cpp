#include "resolutionselector.h"

ResolutionSelector::ResolutionSelector(int tabId, std::string text, Action action, std::string description)
    : GuiElement(GuiElement::Type::ResolutionSelector, tabId, action, std::move(description)), m_text(std::move(text))
{
    m_index = 0;
    m_triggerOnChange = true;
}

sf::Vector2f ResolutionSelector::pos() const
{
    return m_pos;
}

void ResolutionSelector::setPos(const sf::Vector2f &pos)
{
    m_pos = pos;
}

sf::Vector2f ResolutionSelector::size() const
{
    return m_size;
}

void ResolutionSelector::setSize(const sf::Vector2f &size)
{
    m_size = size;
}

bool ResolutionSelector::increment()
{
    if (m_index == m_resolutions.size() - 1) return false;

    m_index++;
    return true;
}

bool ResolutionSelector::decrement()
{
    if (m_index == 0) return false;

    m_index--;
    return true;
}

std::string ResolutionSelector::text() const
{
    return m_text;
}

sf::Vector2u ResolutionSelector::value() const
{
    if (m_resolutions.empty())
        return {};

    return m_resolutions.at(m_index);
}

void ResolutionSelector::setValue(sf::Vector2u resolution)
{
    ptrdiff_t pos = find(m_resolutions.begin(), m_resolutions.end(), resolution) - m_resolutions.begin();

    if(pos < static_cast<int>(m_resolutions.size()))
    {
        m_index = static_cast<unsigned int>(pos);
    }
}

bool ResolutionSelector::triggerOnChange() const
{
    return m_triggerOnChange;
}

void ResolutionSelector::setTriggerOnChange(bool triggerOnChange)
{
    m_triggerOnChange = triggerOnChange;
}

void ResolutionSelector::setResolutions(const std::vector<sf::Vector2u> &resolutions)
{
    m_resolutions = resolutions;
}

