// Author:      Patrick Falkensteiner
// Description: Base class for all gui-elements
//              Holds Type enum that identifies the type of an element
//              Holds Action enum that identifies all possible actions that
//              can be performed by an element

#include "guielement.h"

GuiElement::GuiElement(Type type, int tabId, Action action, std::string description)
    : m_type(type), m_tabId(tabId), m_action(action), m_description(std::move(description))
{

}

GuiElement::Type GuiElement::type() const
{
    return m_type;
}

int GuiElement::tabId() const
{
    return m_tabId;
}

GuiElement::Action GuiElement::action() const
{
    return m_action;
}

std::string GuiElement::description() const
{
    return m_description;
}
