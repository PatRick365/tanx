#include "numberselector.h"

NumberSelector::NumberSelector(int tabId, std::string text, Action action, std::string description)
    : GuiElement(GuiElement::Type::NumberSelector, tabId, action, std::move(description)), m_text(std::move(text))
{
    m_value = 0;
    m_min = 0;
    m_max = 100;
    m_step = 1;

    m_triggerOnChange = true;
}

sf::Vector2f NumberSelector::pos() const
{
    return m_pos;
}

void NumberSelector::setPos(const sf::Vector2f &pos)
{
    m_pos = pos;
}

sf::Vector2f NumberSelector::size() const
{
    return m_size;
}

void NumberSelector::setSize(const sf::Vector2f &size)
{
    m_size = size;
}

bool NumberSelector::increment()
{
    if (m_value == m_max) return false;

    m_value += m_step;

    if (m_value > m_max) m_value = m_max;

    return true;
}

bool NumberSelector::decrement()
{
    if (m_value == m_min) return false;

    m_value -= m_step;

    if (m_value < m_min) m_value = m_min;

    return true;
}

int NumberSelector::value() const
{
    return m_value;
}

void NumberSelector::setValue(int value)
{
    m_value = value;
}

void NumberSelector::setMin(int min)
{
    m_min = min;
}

void NumberSelector::setMax(int max)
{
    m_max = max;
}

void NumberSelector::setStep(int step)
{
    m_step = step;
}

std::string NumberSelector::text() const
{
    return m_text;
}

bool NumberSelector::triggerOnChange() const
{
    return m_triggerOnChange;
}

void NumberSelector::setTriggerOnChange(bool triggerOnChange)
{
    m_triggerOnChange = triggerOnChange;
}
