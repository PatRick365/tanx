#pragma once

#include "guielement.h"

class NumberSelector : public GuiElement
{
public:
    NumberSelector(int tabId, std::string text, Action action, std::string description = std::string());

    sf::Vector2f pos() const;
    void setPos(const sf::Vector2f &pos);

    sf::Vector2f size() const;
    void setSize(const sf::Vector2f &size);

    bool increment();
    bool decrement();

    int value() const;
    void setValue(int value);

    void setMin(int min);
    void setMax(int max);
    void setStep(int step);

    std::string text() const;

    bool triggerOnChange() const;
    void setTriggerOnChange(bool triggerOnChange);

private:
    std::string m_text;

    sf::Vector2f m_pos;
    sf::Vector2f m_size;

    int m_value;
    int m_min;
    int m_max;
    int m_step;

    bool m_triggerOnChange;

};

