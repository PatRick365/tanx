#pragma once

#include "guielement.h"

class ResolutionSelector : public GuiElement
{
public:
    ResolutionSelector(int tabId, std::string text, Action action, std::string description = std::string());

    sf::Vector2f pos() const;
    void setPos(const sf::Vector2f &pos);

    sf::Vector2f size() const;
    void setSize(const sf::Vector2f &size);

    bool increment();
    bool decrement();

    std::string text() const;

    sf::Vector2u value() const;
    void setValue(sf::Vector2u resolution);

    bool triggerOnChange() const;
    void setTriggerOnChange(bool triggerOnChange);

    void setResolutions(const std::vector<sf::Vector2u> &resolutions);



private:
    std::string m_text;

    sf::Vector2f m_pos;
    sf::Vector2f m_size;

    unsigned int m_index;

    std::vector<sf::Vector2u> m_resolutions;

    bool m_triggerOnChange;
};

