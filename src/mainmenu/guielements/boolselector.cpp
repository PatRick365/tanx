#include "boolselector.h"

BoolSelector::BoolSelector(int tabId, std::string text, Action action, std::string description)
    : GuiElement(GuiElement::Type::BoolSelector, tabId, action, std::move(description)), m_text(std::move(text))
{
    m_value = false;
    m_triggerOnChange = true;
}

sf::Vector2f BoolSelector::pos() const
{
    return m_pos;
}

void BoolSelector::setPos(const sf::Vector2f &pos)
{
    m_pos = pos;
}

sf::Vector2f BoolSelector::size() const
{
    return m_size;
}

void BoolSelector::setSize(const sf::Vector2f &size)
{
    m_size = size;
}

void BoolSelector::toggle()
{
    m_value = !m_value;
}

int BoolSelector::value() const
{
    return m_value;
}

void BoolSelector::setValue(int value)
{
    m_value = value;
}

std::string BoolSelector::text() const
{
    return m_text;
}

bool BoolSelector::triggerOnChange() const
{
    return m_triggerOnChange;
}

void BoolSelector::setTriggerOnChange(bool triggerOnChange)
{
    m_triggerOnChange = triggerOnChange;
}
