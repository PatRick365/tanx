#pragma once

#include "guielement.h"

#include <SFML/Graphics.hpp>
#include <string>

class Button : public GuiElement
{
public:
    Button(int tabId, std::string text, Action action, std::string description = std::string());

    std::string text() const;

    sf::Vector2f pos() const;
    void setPos(const sf::Vector2f &pos);

    sf::Vector2f size() const;
    void setSize(const sf::Vector2f &size);

private:
    std::string m_text;

    sf::Vector2f m_pos;
    sf::Vector2f m_size;


};

