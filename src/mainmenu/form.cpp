// Author:      Patrick Falkensteiner
// Description: Represents a page in the menu
//              Holds a number of gui-elements
//              Holds which gui-element is selected

#include "form.h"

#include <iostream>

Form::Form(Type type) : m_type(type)
{
    m_selectedId = 0;

    m_focused = false;

}

void Form::addElement(GuiElement *guiElement)
{
    std::unique_ptr<GuiElement> ptr(guiElement);
    m_guiElements.push_back(std::move(ptr));
}

GuiElement::Action Form::setInput(Form::Input input)
{
    auto &selectedElement = guiElements().at(static_cast<size_t>(m_selectedId)).get();

    switch (input)
    {
    case Form::Up:
        m_selectedId--;
        if (m_selectedId < 0)
            m_selectedId = static_cast<int>(numOfElements()) - 1;
        break;
    case Form::Down:
        m_selectedId++;
        if (m_selectedId >= static_cast<int>(numOfElements()))
            m_selectedId = 0;
        break;
    case Form::Left:
    case Form::Right:
        if (selectedElement.type() == GuiElement::NumberSelector)
        {
            auto &numberSelector = dynamic_cast<NumberSelector &>(selectedElement);
            if (input == Form::Left)
                numberSelector.decrement();
            if (input == Form::Right)
                numberSelector.increment();
            if (numberSelector.triggerOnChange())
                return numberSelector.action();
        }
        else if (selectedElement.type() == GuiElement::BoolSelector)
        {
            auto &boolSelector = dynamic_cast<BoolSelector &>(selectedElement);
            boolSelector.toggle();
            if (boolSelector.triggerOnChange())
                return boolSelector.action();
        }
        else if (selectedElement.type() == GuiElement::ResolutionSelector)
        {
            auto &resolutionSelector = dynamic_cast<ResolutionSelector &>(selectedElement);
            if (input == Form::Left)
                resolutionSelector.decrement();
            if (input == Form::Right)
                resolutionSelector.increment();
            if (resolutionSelector.triggerOnChange())
                return resolutionSelector.action();
        }
        break;
    case Form::Accept:
        if (selectedElement.type() == GuiElement::Button)
        {
            auto &button = dynamic_cast<const Button&>(selectedElement);
            return button.action();
        }
        else if (selectedElement.type() == GuiElement::NumberSelector)
        {
            auto &numberSelector = dynamic_cast<NumberSelector &>(selectedElement);
            if (!numberSelector.triggerOnChange())
                return numberSelector.action();
        }
        else if (selectedElement.type() == GuiElement::BoolSelector)
        {
            auto &boolSelector = dynamic_cast<BoolSelector &>(selectedElement);
            if (!boolSelector.triggerOnChange())
                return boolSelector.action();
        }
        else if (selectedElement.type() == GuiElement::ResolutionSelector)
        {
            auto &resolutionSelector = dynamic_cast<ResolutionSelector &>(selectedElement);
            if (!resolutionSelector.triggerOnChange())
                return resolutionSelector.action();
        }
        break;
    case Form::Back:
        break;
    }
    return Button::ActionNone;
}

std::vector<std::reference_wrapper<GuiElement>> Form::guiElements() const
{
    std::vector<std::reference_wrapper<GuiElement>> guiElementRefs;
    for (auto &ptr : m_guiElements)
    {
        guiElementRefs.push_back(std::ref(*ptr));
    }
    return guiElementRefs;
}

size_t Form::numOfElements() const
{
    return m_guiElements.size();
}

bool Form::focused() const
{
    return m_focused;
}

void Form::setFocused(bool focused)
{
    m_focused = focused;
    if (focused) m_selectedId = 0;

}

int Form::selectedId() const
{
    return m_selectedId;
}

Form::Type Form::type() const
{
    return m_type;
}
