// Author:      Patrick Falkensteiner
// Description: Constructs all menu forms and sets the selected form

#pragma once

#include "form.h"
#include "../settings.h"

#include <vector>

class MenuFactory
{
public:
    static void loadForms(std::vector<Form> &forms, const Settings &settings);
};

