// Author:      Patrick Falkensteiner
// Description: Represents a page in the menu
//              Holds a number of gui-elements
//              Holds which gui-element is selected

#pragma once

#include "guielements/guielement.h"
#include "guielements/button.h"
#include "guielements/numberselector.h"
#include "guielements/boolselector.h"
#include "guielements/resolutionselector.h"

#include <vector>
#include <memory>

class Form
{
public:
    enum Input
    {
        Up,
        Down,
        Left,
        Right,
        Accept,
        Back
    };

    enum Type
    {
        MainMenu,
        SettingsMenu,
        AudioSettingsMenu,
        VideoSettingsMenu,
        GameSettingsMenu
    };


public:
    Form(Type type);

    Type type() const;

    void addElement(GuiElement *guiElement);

    GuiElement::Action setInput(Input input);

    std::vector<std::reference_wrapper<GuiElement>> guiElements() const;

    size_t numOfElements() const;

    bool focused() const;
    void setFocused(bool focused);

    int selectedId() const;


private:
    Type m_type;

    std::vector<std::unique_ptr<GuiElement>> m_guiElements;

    int m_selectedId;

    bool m_focused;


};

