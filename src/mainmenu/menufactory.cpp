// Author:      Patrick Falkensteiner
// Description: Constructs all menu forms and sets the selected form
//              Initializes the settigs menu with values loaded from the Settings object

#include "menufactory.h"
#include "guielements/guielement.h"
#include "guielements/button.h"
#include "guielements/numberselector.h"
#include "guielements/boolselector.h"
#include "guielements/resolutionselector.h"
#include "../globalconst.h"

#include <iostream>

void MenuFactory::loadForms(std::vector<Form> &forms, const Settings &settings)
{
    sf::Vector2f buttonPos(GlobalConst::DIM_WORLD_WIDTH / 2.0f - 100.0f, 300);
    sf::Vector2f buttonSize(200, 50);

    sf::Vector2f buttonWidth2Pos(GlobalConst::DIM_WORLD_WIDTH / 2.0f - 200.0f, 300);
    sf::Vector2f buttonWidth2Size(400, 50);

    // MainForm
    {
        forms.emplace_back(Form(Form::MainMenu));
        int numButtons = 0;

        Button *bGame = new Button(numButtons++, "Game", Button::ActionEnterGameSettings);
        bGame->setPos(buttonPos);

        Button *bSettings = new Button(numButtons++, "Settings", Button::ActionEnterSettings);
        bSettings->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));

        Button *bQuit = new Button(numButtons++, "Quit", Button::ActionQuit);
        bQuit->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));

        forms.back().addElement(bGame);
        forms.back().addElement(bSettings);
        forms.back().addElement(bQuit);
    }
    // Settings
    {
        forms.emplace_back(Form(Form::SettingsMenu));
        int numButtons = 0;

        Button *bVideoSettings = new Button(numButtons++, "Video", Button::ActionEnterVideoSettings);
        bVideoSettings->setPos(buttonPos);

        Button *bAudioSettings = new Button(numButtons++, "Audio", Button::ActionEnterAudioSettings);
        bAudioSettings->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));

        Button *bBack = new Button(numButtons++, "Back", Button::ActionEnterMainMenu);
        bBack->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));

        forms.back().addElement(bVideoSettings);
        forms.back().addElement(bAudioSettings);
        forms.back().addElement(bBack);
    }
    // AudioSettingsMenu
    {
        forms.emplace_back(Form(Form::AudioSettingsMenu));
        int numButtons = 0;

        NumberSelector *bSoundSelector = new NumberSelector(numButtons++, "Sound", Button::ActionChangeSoundVolume, "Change volume of sounds");
        bSoundSelector->setPos(buttonWidth2Pos);
        bSoundSelector->setSize(buttonWidth2Size);
        bSoundSelector->setValue(settings.volumeSound());

        Button *bBack = new Button(numButtons++, "Back", Button::ActionEnterMainMenu, "Save and back to mainmenu");
        bBack->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));
        bBack->setSize(buttonSize);

        forms.back().addElement(bSoundSelector);
        forms.back().addElement(bBack);
    }
    // VideoSettingsMenu
    {
        forms.emplace_back(Form(Form::VideoSettingsMenu));
        int numButtons = 0;

        BoolSelector *bFullscreenSelector = new BoolSelector(numButtons++, "Fullscreen", Button::ActionChangeFullScreen, "Toggle between windowed and fullscreen mode");
        bFullscreenSelector->setPos(buttonWidth2Pos);
        bFullscreenSelector->setSize(buttonWidth2Size);
        bFullscreenSelector->setValue(settings.isFullscreen());

        ResolutionSelector *bResolutionSelector = new ResolutionSelector(numButtons++, "Resolution", Button::ActionChangeResolution, "Press enter to select resolution");
        bResolutionSelector->setPos(sf::Vector2f(buttonWidth2Pos.x, buttonWidth2Pos.y + ((numButtons - 1) * 100)));
        bResolutionSelector->setSize(buttonWidth2Size);
        bResolutionSelector->setTriggerOnChange(false);
        bResolutionSelector->setResolutions(settings.availableResolutions());
        bResolutionSelector->setValue(settings.resolution());

        BoolSelector *bShowFpsSelector = new BoolSelector(numButtons++, "Show Fps", Button::ActionChangeShowFps, "Show an fps counter in-game");
        bShowFpsSelector->setPos(sf::Vector2f(buttonWidth2Pos.x, buttonWidth2Pos.y + ((numButtons - 1) * 100)));
        bShowFpsSelector->setSize(buttonWidth2Size);
        bShowFpsSelector->setValue(settings.showFps());

        Button *bBack = new Button(numButtons++, "Back", Button::ActionEnterMainMenu, "Save and back to mainmenu");
        bBack->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));
        bBack->setSize(buttonSize);

        forms.back().addElement(bFullscreenSelector);
        forms.back().addElement(bResolutionSelector);
        forms.back().addElement(bShowFpsSelector);
        forms.back().addElement(bBack);
    }
    // GameSettingsMenu
    {
        forms.emplace_back(Form(Form::GameSettingsMenu));
        int numButtons = 0;

        Button *bStartGame = new Button(numButtons++, "Start Game", Button::ActionStartGame, "Start the action");
        bStartGame->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));
        bStartGame->setSize(buttonSize);

        NumberSelector *bPlayersHumans = new NumberSelector(numButtons++, "Humans", Button::ActionChangePlayersHuman, "Number of human controlled players");
        bPlayersHumans->setPos(sf::Vector2f(buttonWidth2Pos.x, buttonWidth2Pos.y + ((numButtons - 1) * 100)));
        bPlayersHumans->setSize(buttonWidth2Size);
        bPlayersHumans->setValue(settings.playersHuman());
        bPlayersHumans->setMin(0);
        bPlayersHumans->setMax(2);

        NumberSelector *bPlayers = new NumberSelector(numButtons++, "Players", Button::ActionChangePlayers, "Number of players overall");
        bPlayers->setPos(sf::Vector2f(buttonWidth2Pos.x, buttonWidth2Pos.y + ((numButtons - 1) * 100)));
        bPlayers->setSize(buttonWidth2Size);
        bPlayers->setValue(settings.players());
        bPlayers->setMin(0);
        bPlayers->setMax(4);

        Button *bBack = new Button(numButtons++, "Back", Button::ActionEnterMainMenu, "Back to mainmenu");
        bBack->setPos(sf::Vector2f(buttonPos.x, buttonPos.y + ((numButtons - 1) * 100)));
        bBack->setSize(buttonSize);

        forms.back().addElement(bStartGame);
        forms.back().addElement(bPlayersHumans);
        forms.back().addElement(bPlayers);
        forms.back().addElement(bBack);
    }


    forms.front().setFocused(true);
}
