// Author:      Patrick Falkensteiner
// Description: Manages menuing system
//              Holds all menu forms
//              Handles input and passes it on to the active form
//              Performs actions according to gui-element interactions like altering the Settings object,
//                  navigate the gui or setting the start-game-flag
//              Renders active form

#include "mainmenu.h"
#include "../application.h"
#include "../globalconst.h"
#include "menufactory.h"

#include "guielements/guielement.h"
#include "guielements/button.h"
#include "guielements/numberselector.h"
#include "guielements/boolselector.h"
#include "guielements/resolutionselector.h"

#include <iostream>
#include <exception>

MainMenu::MainMenu(sf::RenderWindow &window, Settings &settings, SoundManager &soundManager)
    : m_window(window), m_settings(settings), m_soundManager(soundManager)
{
    sf::View view(sf::FloatRect(sf::Vector2f(0, 0), static_cast<sf::Vector2f>(settings.resolution())));

    m_background.loadFromFile("resources/menu/background.jpg");

    MenuFactory::loadForms(m_forms, m_settings);

    m_startGameFlag = false;
}

void MainMenu::update()
{
    handleInput();
    render();
}

void MainMenu::handleInput()
{
    sf::Event event;

    m_releasedKeys.clear();

    unsigned int indexForm = 0;
    for (unsigned int i = 0; i < m_forms.size(); ++i)
    {
        if (m_forms.at(i).focused())
        {
            indexForm = i;
            break;
        }
    }

    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            m_window.close();

        if (event.type == sf::Event::KeyPressed)
        {
            m_pressedKeys.insert(event.key.code);
        }
        else if (event.type == sf::Event::KeyReleased)
        {
            m_releasedKeys.insert(event.key.code);
            m_pressedKeys.erase(event.key.code);
        }

        if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
            case sf::Keyboard::Escape:
                m_window.close();
                break;
            case sf::Keyboard::Up:
                m_forms[indexForm].setInput(Form::Up);
                break;
            case sf::Keyboard::Down:
                m_forms[indexForm].setInput(Form::Down);
                break;
            case sf::Keyboard::Left:
            {
                auto action = m_forms[indexForm].setInput(Form::Left);
                performAction(action);
                break;
            }
            case sf::Keyboard::Right:
            {
                auto action = m_forms[indexForm].setInput(Form::Right);
                performAction(action);
                break;
            }
            case sf::Keyboard::Return:
            case sf::Keyboard::Space:
            {
                auto action = m_forms[indexForm].setInput(Form::Accept);
                performAction(action);
                break;
            }
            default:
                break;
            }
        }
    }

    if (m_releasedKeys.count(m_settings.keyRes0()))
        m_settings.setResolution(sf::Vector2u(1024, 576));
    else if (m_releasedKeys.count(m_settings.keyRes1()))
        m_settings.setResolution(sf::Vector2u(1152, 648));
    else if (m_releasedKeys.count(m_settings.keyRes2()))
        m_settings.setResolution(sf::Vector2u(1280, 720));
    else if (m_releasedKeys.count(m_settings.keyRes3()))
        m_settings.setResolution(sf::Vector2u(1366, 768));
    else if (m_releasedKeys.count(m_settings.keyRes4()))
        m_settings.setResolution(sf::Vector2u(1600, 900));
    else if (m_releasedKeys.count(m_settings.keyRes5()))
        m_settings.setResolution(sf::Vector2u(1920, 1080));
    if (m_releasedKeys.count(m_settings.keyFullscreen()))
        m_settings.setIsFullscreen(!m_settings.isFullscreen());

}


void MainMenu::render()
{
    sf::Vector2f scale(m_settings.resolution().x / static_cast<float>(GlobalConst::DIM_WORLD_WIDTH - 20),    // WHY 20 px??
                       m_settings.resolution().y / static_cast<float>(GlobalConst::DIM_WORLD_HEIGHT - 30));  // WHY 30 px??

    sf::Sprite background;
    background.setTexture(m_background);
    background.setScale(scale);

    sf::RectangleShape buttonRect;
    buttonRect.setOutlineThickness(3 * scale.x);
    buttonRect.setFillColor(sf::Color::Transparent);

    sf::Font font;
    font.loadFromFile("resources/fonts/Capture_it.ttf");
    sf::Text buttonText("", font, 30);

    m_window.clear();

    m_window.draw(background);

    for (auto &form : m_forms)
    {
        if (!form.focused()) continue;

        for (auto element : form.guiElements())
        {
            sf::Color col = sf::Color::Black;

            if (element.get().tabId() == form.selectedId())
            {
                col = sf::Color::Red;

                sf::Text descriptionText(element.get().description(), font, 30 /* scale.x*/);

                sf::Vector2f pos;

                pos.x = GlobalConst::DIM_WORLD_WIDTH - descriptionText.getLocalBounds().width - 100;
                pos.y = 50;

                descriptionText.setPosition(pos.x * scale.x, pos.y * scale.y);

                descriptionText.setScale(scale);
                descriptionText.setFillColor(col);
                m_window.draw(descriptionText);
            }

            buttonRect.setOutlineColor(col);

            GuiElement::Type type = element.get().type();

            switch (type)
            {
            case GuiElement::Type::Button:
            {
                auto &button = dynamic_cast<const Button &>(element.get());
                buttonText.setString(button.text());
                buttonText.setPosition(sf::Vector2f((button.pos().x + 5) * scale.x, (button.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                buttonText.setFillColor(col);
                m_window.draw(buttonText);
                break;
            }
            case GuiElement::Type::NumberSelector:
            {
                auto &numberSelector = dynamic_cast<const NumberSelector &>(element.get());
                buttonText.setString(numberSelector.text());
                buttonText.setPosition(sf::Vector2f((numberSelector.pos().x + 5) * scale.x, (numberSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                buttonText.setFillColor(col);
                m_window.draw(buttonText);

                buttonText.setString(std::to_string(numberSelector.value()));
                buttonText.setPosition(sf::Vector2f((numberSelector.pos().x + 5 + numberSelector.size().x / 2) * scale.x, (numberSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                m_window.draw(buttonText);

                break;
            }
            case GuiElement::Type::BoolSelector:
            {
                auto &boolSelector = dynamic_cast<const BoolSelector &>(element.get());
                buttonText.setString(boolSelector.text());
                buttonText.setPosition(sf::Vector2f((boolSelector.pos().x + 5) * scale.x, (boolSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                buttonText.setFillColor(col);
                m_window.draw(buttonText);

                buttonText.setString(boolSelector.value() ? "ON" : "OFF");
                buttonText.setPosition(sf::Vector2f((boolSelector.pos().x + 5 + boolSelector.size().x / 2) * scale.x, (boolSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                m_window.draw(buttonText);

                break;
            }
            case GuiElement::Type::ResolutionSelector:
            {
                auto &resolutionSelector = dynamic_cast<const ResolutionSelector &>(element.get());
                buttonText.setString(resolutionSelector.text());
                buttonText.setPosition(sf::Vector2f((resolutionSelector.pos().x + 5) * scale.x, (resolutionSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                buttonText.setFillColor(col);
                m_window.draw(buttonText);

                buttonText.setString(std::to_string(resolutionSelector.value().x) + "x" + std::to_string(resolutionSelector.value().y));
                buttonText.setPosition(sf::Vector2f((resolutionSelector.pos().x + 5 + resolutionSelector.size().x / 2) * scale.x, (resolutionSelector.pos().y + 5) * scale.y));
                buttonText.setScale(scale);
                m_window.draw(buttonText);

                break;
            }

            }
        }
    }

    m_window.display();
}

void MainMenu::performAction(GuiElement::Action action)
{
    switch (action)
    {
    case GuiElement::ActionStartGame:
        m_startGameFlag = true;
        break;
    case GuiElement::ActionQuit:
        m_window.close();
        break;
    case GuiElement::ActionEnterSettings:
        focusForm(Form::SettingsMenu);
        break;
    case GuiElement::ActionEnterGameSettings:
        focusForm(Form::GameSettingsMenu);
        break;
    case GuiElement::ActionEnterMainMenu:
        m_settings.saveToConfig();
        focusForm(Form::MainMenu);
        break;
    case GuiElement::ActionEnterVideoSettings:
        focusForm(Form::VideoSettingsMenu);
        break;
    case GuiElement::ActionEnterAudioSettings:
        focusForm(Form::AudioSettingsMenu);
        break;
    case GuiElement::ActionChangeSoundVolume:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::NumberSelector)
        {
            auto numberSelector = dynamic_cast<const NumberSelector *>(&element);
            int volume = numberSelector->value();
            m_settings.setVolumeSound(volume);
            m_soundManager.playSoundExplosion();
        }
        break;
    }
    case GuiElement::ActionChangeFullScreen:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::BoolSelector)
        {
            auto boolSelector = dynamic_cast<const BoolSelector *>(&element);
            bool fullscreen = boolSelector->value();
            m_settings.setIsFullscreen(fullscreen);
        }
        break;
    }
    case GuiElement::ActionChangeShowFps:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::BoolSelector)
        {
            auto boolSelector = dynamic_cast<const BoolSelector *>(&element);
            bool showFps = boolSelector->value();
            m_settings.setShowFps(showFps);
        }
        break;
    }
    case GuiElement::ActionChangeResolution:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::ResolutionSelector)
        {
            auto resolutionSelector = dynamic_cast<const ResolutionSelector *>(&element);
            sf::Vector2u resolution = resolutionSelector->value();
            m_settings.setResolution(resolution);
        }
        break;
    }
    case GuiElement::ActionChangePlayersHuman:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::NumberSelector)
        {
            auto numberSelector = dynamic_cast<const NumberSelector *>(&element);
            int value = numberSelector->value();
            m_settings.setPlayersHuman(value);
        }
        break;
    }
    case GuiElement::ActionChangePlayers:
    {
        const GuiElement &element = getSelectedGuiElement();
        if (element.type() == GuiElement::Type::NumberSelector)
        {
            auto numberSelector = dynamic_cast<const NumberSelector *>(&element);
            int value = numberSelector->value();
            m_settings.setPlayers(value);
        }
        break;
    }
    case GuiElement::ActionNone:
        break;
    }
}

void MainMenu::focusForm(Form::Type formType)
{
    for (auto &form : m_forms)
    {
        form.setFocused(form.type() == formType);
    }
}

GuiElement &MainMenu::getSelectedGuiElement()
{
    for (auto &form : m_forms)
    {
        if (!form.focused()) continue;
        for (auto element : form.guiElements())
        {
            if (!(element.get().tabId() == form.selectedId())) continue;
            return element.get();
        }
    }
    throw std::logic_error("no form and/or guielement set as selected.\n");
}

bool MainMenu::queryStartGameFlag()
{
    const bool flag = m_startGameFlag;
    m_startGameFlag = false;
    return flag;
}





