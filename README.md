Tanx is a game about tanks.

It was realized in SFML2.


Features:
- Proven architecture (GOD class)
- Destructible environment. (only 1 level, sorry)
- Accurate, although poorly implemented collision detection.
- Clunkyly (is that a word?) implemented UI-system
- Works with 0 to 4 players, 0 to 2 of them can be humans.
- The stupidity of the AI is partly overcome by its accuracy when aiming.
- "Nice" particle effects.
- Explosions

![]( screenshots/tanx_menu.png )
![]( screenshots/tanx_ingame.png )


Building:
    
tested only on linux
    
have sfml2 installed
    
enter project dir and run:
    
    cmake CMakeLists.txt
    make
    ./tanx

controls ( at least for german keyboard :-/ ):

    player 1:
        arrowkeys drive
        ,- turn turret
        . shoot
        
    player 2:
        wasd drive
        fh turn turret
        g shoot
