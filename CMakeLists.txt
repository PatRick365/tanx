cmake_minimum_required(VERSION 2.8.9)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

project(Tanx)

find_package(SFML 2.5 COMPONENTS graphics audio REQUIRED)

file(GLOB_RECURSE source_list "src/*.cpp")

add_executable(tanx ${source_list})

target_link_libraries(tanx sfml-graphics sfml-audio)


